import 'package:flutter/material.dart';
import 'package:igplaner_mobile/controllers/login/loginDataController.dart';
import 'package:igplaner_mobile/controllers/scaffold/appMaterialApp.dart';

import '../../widgets/BorderedListTile.dart';
import '../../models/UserModel.dart';

enum CustomHeaderType { standard, user, none }

class CustomHeader {
  final CustomHeaderType type;
  final Widget title;
  final ImageProvider backgroundImage;

  CustomHeader(
      {this.type = CustomHeaderType.none, this.title, this.backgroundImage});
}

class CustomDrawerController {
  CustomHeader header;
  List<DrawerEntry> mainContent;
  List<DrawerEntry> bottomContent;

  CustomDrawerController(
      {this.header,
      this.mainContent = const [],
      this.bottomContent = const []}) {
    if (header == null) header = CustomHeader();
  }

  Widget widgetTree(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    return Column(
      children: <Widget>[
        headerWidget(context, navigatorKey: navigatorKey),
        Expanded(
          child: SafeArea(
            bottom: false,
            right: false,
            left: false,
            top: true,
            child: SingleChildScrollView(
                child: mainWidget(context, navigatorKey: navigatorKey)),
          ),
        ),
        this.bottomContent != null ? Divider() : Container(),
        this.bottomContent != null
            ? bottomWidget(context, navigatorKey: navigatorKey)
            : Container()
      ],
    );
  }

  Drawer getDrawer(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    return Drawer(
      child: widgetTree(context, navigatorKey: navigatorKey),
    );
  }

  Widget headerWidget(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    return header.type == CustomHeaderType.standard
        ? DrawerHeader(
            child: header.title,
            decoration: BoxDecoration(
                image: header.backgroundImage != null
                    ? DecorationImage(
                        image: header.backgroundImage,
                        fit: BoxFit.cover,
                        alignment: Alignment.center)
                    : null),
          )
        : header.type == CustomHeaderType.user &&
                LoginDataController.getLoginStatus(context) ==
                    LoginStatus.loggedIn
            ? UserAccountsDrawerHeader(
                accountEmail:
                    Text(LoginDataController.getActiveUser(context).userEmail),
                accountName:
                    Text(LoginDataController.getActiveUser(context).userName),
                currentAccountPicture:
                    LoginDataController.getActiveUser(context).userImage,
                decoration: BoxDecoration(
                    image: DecorationImage(image: header.backgroundImage)),
                otherAccountsPictures:
                    LoginDataController.getInactiveUsers(context)
                        .map(
                          (User user) => GestureDetector(
                              child: user.userImage, onTap: () => {}),
                        )
                        .toList(),
                onDetailsPressed: () {
                  print("Test");
                },
              )
            : Container();
  }

  Widget mainWidget(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    return Column(
        children: mainContent
            .map((DrawerEntry entry) =>
                entry.widget(context, navigatorKey: navigatorKey))
            .toList());
  }

  Widget bottomWidget(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    return Column(
        children: bottomContent
            .map((DrawerEntry entry) =>
                entry.widget(context, navigatorKey: navigatorKey))
            .toList());
  }
}

class DrawerEntry {
  final String text;
  Function(BuildContext) onTap;
  final IconData icon;
  final String namedRoute;
  bool _bordered = false;

  DrawerEntry({this.text, this.namedRoute = "/", this.onTap, this.icon});
  DrawerEntry.bordered(
      {this.text, this.namedRoute = "/", this.onTap, this.icon})
      : _bordered = true;

  Widget widget(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    this.onTap ??= (BuildContext context) =>
        navigatorKey.currentState.pushReplacementNamed(namedRoute);
    return _bordered
        ? Padding(
            padding: EdgeInsets.only(right: 20),
            child: BorderedListTile(
              borderRadius:
                  BorderRadius.horizontal(right: Radius.circular(200)),
              title: text,
              leading: icon,
              onTap: () => onTap(context),
            ))
        : ListTile(
            leading: Icon(icon),
            title: Text(text),
            onTap: () => onTap(context),
          );
  }
}

class CustomDrawerProvider extends InheritedWidget {
  final CustomDrawerController controller;
  CustomDrawerProvider({this.controller, Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static Widget of(BuildContext context,
      {GlobalKey<NavigatorState> navigatorKey}) {
    navigatorKey ??= NavigatorKeyProvider.of(context);
    return context
        .dependOnInheritedWidgetOfExactType<CustomDrawerProvider>()
        .controller
        .getDrawer(context, navigatorKey: navigatorKey);
  }

  static drawerControllerOf(BuildContext context) => context
      .dependOnInheritedWidgetOfExactType<CustomDrawerProvider>()
      .controller;
}
