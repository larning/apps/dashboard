import 'package:flutter/material.dart';
import '../../users/userDataController.dart';

import '../drawerController.dart';

class UserControlledDrawerEntry extends DrawerEntry {
  final BuildContext context;
  final List<String> permissionsRequired;
  final List<String> permissionRequired;

  UserControlledDrawerEntry(
      {String text,
      String namedRoute = "/",
      Function(BuildContext) onTap,
      IconData icon,
      this.context,
      this.permissionsRequired,
      this.permissionRequired})
      : super(text: text, namedRoute: namedRoute, onTap: onTap, icon: icon);

  UserControlledDrawerEntry.bordered(
      {String text,
      String namedRoute = "/",
      Function(BuildContext) onTap,
      IconData icon,
      this.context,
      this.permissionsRequired,
      this.permissionRequired})
      : super.bordered(
            text: text, namedRoute: namedRoute, onTap: onTap, icon: icon);

  Widget widget(BuildContext context,
          {@required GlobalKey<NavigatorState> navigatorKey}) =>
      UserDataProvider.of(context).loggedInUser?.permissions == null ||
              !((permissionsRequired != null &&
                      permissionsRequired.every((String permissions) =>
                          UserDataProvider.of(context)
                              .loggedInUser
                              .permissions
                              .contains(permissions))) ||
                  (permissionRequired != null &&
                      permissionRequired.any((String permission) =>
                          UserDataProvider.of(context)
                              .loggedInUser
                              .permissions
                              .contains(permission))))
          ? Container()
          : super.widget(context, navigatorKey: navigatorKey);
}
