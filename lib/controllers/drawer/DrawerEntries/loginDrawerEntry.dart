import 'package:flutter/material.dart';
import 'package:igplaner_mobile/controllers/login/loginDataController.dart';

import '../drawerController.dart';

class LoginDrawerEntry {
  String loggedInText, loggedOutText, logInNamedRoute, logOutNamedRoute;
  Function(BuildContext) onLoginTap, onLogoutTap;
  IconData loggedInIcon, loggedOutIcon;
  BuildContext buildContext;
  bool _bordered = false;

  LoginDrawerEntry(
      {@required this.loggedInText,
      @required this.loggedOutText,
      this.onLoginTap,
      this.onLogoutTap,
      this.logInNamedRoute = "/",
      this.logOutNamedRoute = "/",
      @required this.loggedInIcon,
      @required this.loggedOutIcon,
      @required this.buildContext});
  LoginDrawerEntry.bordered(
      {@required this.loggedInText,
      @required this.loggedOutText,
      this.onLoginTap,
      this.onLogoutTap,
      this.logInNamedRoute = "/",
      this.logOutNamedRoute = "/",
      @required this.loggedInIcon,
      @required this.loggedOutIcon,
      @required this.buildContext})
      : _bordered = true;

  DrawerEntry loginDependant(BuildContext context) => _bordered
      ? _LogInDependant.bordered(
          context: context,
          onLoginTap: onLoginTap,
          logInNamedRoute: logInNamedRoute,
          loggedOutIcon: loggedOutIcon,
          loggedOutText: loggedOutText)
      : _LogInDependant(
          context: context,
          onLoginTap: onLoginTap,
          logInNamedRoute: logInNamedRoute,
          loggedOutIcon: loggedOutIcon,
          loggedOutText: loggedOutText);

  DrawerEntry logOutDependant(BuildContext context) => _bordered
      ? _LogOutDependant.bordered(
          context: context,
          onLogoutTap: onLogoutTap,
          logOutNamedRoute: logOutNamedRoute,
          loggedInIcon: loggedInIcon,
          loggedInText: loggedInText)
      : _LogOutDependant(
          context: context,
          onLogoutTap: onLogoutTap,
          logOutNamedRoute: logOutNamedRoute,
          loggedInIcon: loggedInIcon,
          loggedInText: loggedInText);
}

class _LogInDependant extends DrawerEntry {
  BuildContext context;
  String loggedOutText, logInNamedRoute;
  Function(BuildContext) onLoginTap;
  IconData loggedOutIcon;

  _LogInDependant(
      {@required this.context,
      @required this.loggedOutText,
      @required this.onLoginTap,
      @required this.loggedOutIcon,
      @required this.logInNamedRoute})
      : super(
            text: loggedOutText,
            namedRoute: logInNamedRoute,
            icon: loggedOutIcon,
            onTap: onLoginTap);
  _LogInDependant.bordered(
      {@required this.context,
      @required this.loggedOutText,
      @required this.onLoginTap,
      @required this.loggedOutIcon,
      @required this.logInNamedRoute})
      : super.bordered(
            text: loggedOutText,
            namedRoute: logInNamedRoute,
            icon: loggedOutIcon,
            onTap: onLoginTap);

  Widget widget(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    LoginStatus loggedIn = LoginDataController.getLoginStatus(context);
    return loggedIn == LoginStatus.loggedIn
        ? Container()
        : super.widget(context, navigatorKey: navigatorKey);
  }
}

class _LogOutDependant extends DrawerEntry {
  BuildContext context;
  String loggedInText, logOutNamedRoute;
  Function(BuildContext) onLogoutTap;
  IconData loggedInIcon;

  _LogOutDependant(
      {@required this.context,
      @required this.loggedInText,
      @required this.onLogoutTap,
      @required this.loggedInIcon,
      @required this.logOutNamedRoute})
      : super(
            text: loggedInText,
            namedRoute: logOutNamedRoute,
            icon: loggedInIcon,
            onTap: onLogoutTap);

  _LogOutDependant.bordered(
      {@required this.context,
      @required this.loggedInText,
      @required this.onLogoutTap,
      @required this.loggedInIcon,
      @required this.logOutNamedRoute})
      : super.bordered(
            text: loggedInText,
            namedRoute: logOutNamedRoute,
            icon: loggedInIcon,
            onTap: onLogoutTap);

  Widget widget(BuildContext context,
      {@required GlobalKey<NavigatorState> navigatorKey}) {
    LoginStatus loggedIn = LoginDataController.getLoginStatus(context);
    return loggedIn == LoginStatus.loggedOut
        ? Container()
        : super.widget(context, navigatorKey: navigatorKey);
  }
}
