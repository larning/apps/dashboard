import 'package:flutter/material.dart';

class SlideRoute extends PageRouteBuilder {
  final Widget page;
  final SlideDirection direction;
  SlideRoute({@required this.page, @required this.direction})
      : super(
            pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) =>
                page,
            transitionsBuilder: (BuildContext context,
                    Animation<double> animation,
                    Animation<double> secondaryAnimation,
                    Widget child) =>
                SlideTransition(
                  position: Tween<Offset>(
                          begin: direction == SlideDirection.up
                              ? Offset(0, 1)
                              : direction == SlideDirection.down
                                  ? Offset(0, -1)
                                  : direction == SlideDirection.left
                                      ? Offset(1, 0)
                                      : direction == SlideDirection.right
                                          ? Offset(-1, 0)
                                          : throw new Error(),
                          end: Offset.zero)
                      .animate(animation),
                  child: child,
                ));
}

enum SlideDirection { right, left, up, down }
