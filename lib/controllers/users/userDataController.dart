import 'package:flutter/material.dart';
import '../../models/UserModel.dart';

class UserDataController extends ChangeNotifier {
  List<User> registeredUsers = [];
  User loggedInUser;

  void setUser(User user) {
    loggedInUser = user;
    print(user?.id?.id);

    notifyListeners();
  }
}

class UserDataProvider extends InheritedNotifier {
  final UserDataController controller;
  UserDataProvider({Widget child, this.controller}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static UserDataController of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<UserDataProvider>().controller;
}
