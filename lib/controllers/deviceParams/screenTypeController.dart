import 'package:flutter/material.dart';

enum ScreenType { Phone, Phablet, Notebook }

class ScreenTypeProvider extends InheritedWidget {
  final ScreenType screenType;

  static ScreenType initailzeScreenType(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return width < 600
        ? ScreenType.Phone
        : width < 720 ? ScreenType.Phablet : ScreenType.Notebook;
  }

  ScreenTypeProvider({BuildContext context, Widget child})
      : screenType = initailzeScreenType(context),
        super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static ScreenType of(BuildContext context) => context
      .dependOnInheritedWidgetOfExactType<ScreenTypeProvider>()
      .screenType;
}
