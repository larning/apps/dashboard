import 'package:flutter/material.dart';

class UrlProvider extends InheritedWidget {
  final String url;
  UrlProvider({Widget child, this.url}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  static String of(BuildContext context){
    final String mainUrl = context.dependOnInheritedWidgetOfExactType<UrlProvider>().url;
    return (mainUrl.endsWith("/")? mainUrl: mainUrl + "/");
  }

  static String subPage(BuildContext context, List<String> url){
    final String mainUrl = context.dependOnInheritedWidgetOfExactType<UrlProvider>().url;
    return (mainUrl.endsWith("/")? mainUrl: mainUrl + "/") + url.reduce((value, element) => value + "/" + element);
  }
       
}
