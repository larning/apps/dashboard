import 'package:flutter/material.dart';

class MapsController {
  static const int MAX_ZOOM = 17;
  static const int MIN_ZOOM = 1;
  static const int MAX_WIDTH = 512;
  static const int MAX_HEIGHT = 512;
  static const int MIN_WIDTH = 64;
  static const int MIN_HEIGHT = 64;

  final String apiKey;
  MapsController({@required this.apiKey});

  String staticImageUrl(
      {@required double longitude,
      @required double latitude,
      int zoom = 17,
      bool useThemeBrightness = true,
      bool dark = false,
      int width = 512,
      int height = 512,
      BuildContext context}) {
    // Getting zoom into map provider bounds
    zoom = zoom > MAX_ZOOM ? MAX_ZOOM : zoom < MIN_ZOOM ? MIN_ZOOM : zoom;

    // Getting width & height into map provider bounds while in the same aspect ratio
    int originalWidth = width;
    width = width > MAX_WIDTH
        ? (width > height ? MAX_WIDTH : (width / height) * MAX_WIDTH)
        : width;
    height = height > MAX_HEIGHT
        ? (height > originalWidth
            ? MAX_HEIGHT
            : (height / originalWidth) * MAX_HEIGHT)
        : height;
    width = width < MIN_WIDTH ? MIN_WIDTH : width;
    height = height < MIN_HEIGHT ? MIN_HEIGHT : height;

    // determining if we should use the dark theme
    dark = useThemeBrightness
        ? Theme.of(context).brightness == Brightness.dark
        : dark;

    return "https://stadiamaps.com/static/alidade_smooth${dark ? '_dark' : ''}?api_key=$apiKey&center=${longitude.toString()},${latitude.toString()}&zoom=${zoom.toString()}&size=${width.toString()}x${height.toString()}";
  }
}

class MapsProvider extends InheritedWidget {
  final MapsController mapsController;

  MapsProvider({Widget child, this.mapsController}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  static MapsController of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<MapsProvider>().mapsController;
}
