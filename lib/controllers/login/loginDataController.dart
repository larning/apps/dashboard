import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import '../users/userDataController.dart';
import '../../models/UserModel.dart';
import '../network/urlController.dart';

enum LoginStatus { loggedIn, loggedOut }

class LoginDataController {
  static LoginStatus getLoginStatus(BuildContext context) =>
      UserDataProvider.of(context).loggedInUser == null
          ? LoginStatus.loggedOut
          : LoginStatus.loggedIn;

  static User getActiveUser(BuildContext context) =>
      UserDataProvider.of(context).loggedInUser;

  static List<User> getInactiveUsers(BuildContext context) =>
      UserDataProvider.of(context)
          .registeredUsers
          .where((User user) => user != getActiveUser(context))
          .toList();

  static Future<File> _getLocalFile(String filename) async {
    String dir = (await pathProvider.getTemporaryDirectory()).path;
    File f = new File('$dir/$filename');
    return f;
  }

  static Future<bool> signup(
      {@required String username,
      @required String password,
      @required String realName,
      @required String email,
      @required List<String> pictureUrls}) async {
    final List<String> pictureUrlsWithoutNull =
        pictureUrls.where((String pictureUrl) => pictureUrl != null).toList();
    List<File> pictures = <File>[];
    for (String pictureUrl in pictureUrlsWithoutNull) {
      pictures.add(await _getLocalFile(pictureUrl));
    }
    print(pictures.toString());
    return false;
  }

  static Future<bool> login(String usernameOrEmail, String password,
      {@required BuildContext context}) async {
    print(UrlProvider.of(context)
        .split("https://")[1]
        .split("/")[0]
        .split(":")[0]);
    http.Client noValidateClient() {
      var ioClient = new HttpClient()
        ..badCertificateCallback = (cert, host, _) =>
            host ==
            UrlProvider.of(context)
                .split("https://")[1]
                .split("/")[0]
                .split(":")[0];

      return new IOClient(ioClient);
    }

    var client = noValidateClient();
    final http.Response response = await client.post(
      UrlProvider.subPage(context, ["users", "login"]),
      headers: {"Content-Type": " application/json"},
      body: json.encode(
        {
          "username": usernameOrEmail.contains("@") ? null : usernameOrEmail,
          "email": usernameOrEmail.contains("@") ? usernameOrEmail : null,
          "password": password
        },
      ),
    );
    client.close();
    if (response.statusCode != 200) return false;
    final Map body = json.decode(response.body);
    if (body["message"] != "AUTH_SUCCESS") return false;
    UserDataProvider.of(context).setUser(User(
        userName: body["user"]["username"],
        id: UserId(body["user"]["_id"]),
        userEmail: body["user"]["email"],
        permissions: List<String>.from(body["user"]["permissions"])));
    return true;
  }

  static String usernameValidator(String username) {
    if (username.isEmpty) return "Please provide an username";
    if (username.length < 8) return "8 Characters required.";
    return null; //TODO: Remove placeholder @low
  }

  static String emailUsernameValidator(String emailOrUsername) {
    if (emailOrUsername.isEmpty)
      return "You have to prvide an username or email";
    if (emailOrUsername.contains("@") &&
        (RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$")
                .firstMatch(emailOrUsername) ==
            null)) return "Please provide an valid Email.";
    if (emailOrUsername.length < 8 && !emailOrUsername.contains("@"))
      return "8 Characters required";
    return null; //TODO: Remove placeholder @low
  }

  static String passwordValidator(String password) {
    if (password.isEmpty) return "Please provide an password.";
    if (password.length < 8) return "8 Characters required";
    return null; //TODO: Remove placeholder @low
  }

  static String repeatedPasswordValidator(
      String password, String repeatedPassword) {
    if (repeatedPassword.isEmpty) return "Please retype the password.";
    print(password);
    print(repeatedPassword);
    if (password != repeatedPassword) return "Passwords do not match";
    return null;
  }

  static String emailValidator(String email) {
    if (email.isEmpty) return "Please provide an email.";
    if (RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$")
            .firstMatch(email) ==
        null) return "Please provide an valid Email";
    return null; //TODO: Remove placeholder @low
  }

  static String nameValidator(String realName) {
    if (!realName.trim().contains(" "))
      return "Please provide an forename and an surname.";
    if (realName.length < 5)
      return "Nobody has an forename and an surname with together only 5 characters.";
    return null; //TODO: Remove placeholder @low
  }
}
