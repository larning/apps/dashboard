import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../network/urlController.dart';

/* {
    "SSID": "IGP_WLAN",
    "USERNAME": "honeymelon",
    "PASSWORD": "D4sP4ss\\/\\/0rd12345678",
    "EXPIRED": false,
    "EXPIRES_IN": 2000,
    "DEACTIVATION_MESSAGE": ""
   } */

class WifiDataController {
  static Future<List<Map<String, dynamic>>> getWifiData(
      BuildContext context) async {
    /*http.Response response = await http.get(
      UrlProvider.of(context) + "radius/credentials",
    );*/
    return [
      {
        "SSID": "IGP_WLAN",
        "USERNAME": "honeymelon",
        "PASSWORD": "D4sP4ss\\/\\/0rd12345678",
        "EXPIRED": false,
        "EXPIRES_IN": 2000,
        "DEACTIVATION_MESSAGE": "",
        "COORDINATES": {"LONGITUDE": 50.997682, "LATITUDE": 7.096352}
      },
      {
        "SSID": "IGP_WLAN",
        "USERNAME": "honeymelon",
        "PASSWORD": "D4sP4ss\\/\\/0rd12345678",
        "EXPIRED": false,
        "EXPIRES_IN": 2000,
        "DEACTIVATION_MESSAGE": "",
        "COORDINATES": {"LONGITUDE": 50.997682, "LATITUDE": 7.096352}
      }
    ];
  }
}
