import 'package:flutter/material.dart';
import 'package:igplaner_mobile/controllers/drawer/drawerController.dart';
import 'package:igplaner_mobile/controllers/scaffold/appMaterialApp.dart';

import '../deviceParams/screenTypeController.dart';

class AppScaffold extends StatelessWidget {
  final PreferredSizeWidget Function(BuildContext context) appBar;
  final Color backgroundColor;
  final Widget body,
      bottomNavigationBar,
      bottomSheet,
      endDrawer,
      floatingActionButton;
  final bool displayDrawer,
      extendBody,
      extendBodyBehindAppBar,
      primary,
      resizeToAvoidBottomInset,
      subPage;
  final FloatingActionButtonAnimator floatingActionButtonAnimator;
  final FloatingActionButtonLocation floatingActionButtonLocation;
  final List<Widget> persistentFooterButtons;
  const AppScaffold({
    Key key,
    this.appBar,
    this.backgroundColor,
    this.body,
    this.bottomNavigationBar,
    this.bottomSheet,
    this.endDrawer,
    this.floatingActionButton,
    this.displayDrawer = true,
    this.extendBody = false,
    this.extendBodyBehindAppBar = false,
    this.primary = true,
    this.resizeToAvoidBottomInset = true,
    this.floatingActionButtonAnimator,
    this.floatingActionButtonLocation,
    this.persistentFooterButtons,
    this.subPage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ScreenType screenType = ScreenTypeProvider.of(context);

    RefreshDrawerProvider.update(context,
        displayDrawer: displayDrawer, subPage: subPage);

    return Row(
      children: <Widget>[
        Expanded(
          child: Scaffold(
            appBar: appBar == null ? null : appBar(context),
            drawer: screenType == ScreenType.Phone && displayDrawer && !subPage
                ? CustomDrawerProvider.of(context)
                : null,
            backgroundColor: backgroundColor,
            body: body,
            bottomNavigationBar: bottomNavigationBar,
            bottomSheet: bottomSheet,
            endDrawer: endDrawer,
            floatingActionButton: floatingActionButton,
            extendBody: extendBody,
            extendBodyBehindAppBar: extendBodyBehindAppBar,
            primary: primary,
            resizeToAvoidBottomInset: resizeToAvoidBottomInset,
            floatingActionButtonAnimator: floatingActionButtonAnimator,
            floatingActionButtonLocation: floatingActionButtonLocation,
            persistentFooterButtons: persistentFooterButtons,
          ),
        ),
      ],
    );
  }
}
