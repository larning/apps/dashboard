import 'package:flutter/material.dart';
import '../localization/localizations.dart';

class SecondsToString {
  int _allSeconds;
  BuildContext _context;
  int seconds;
  int minutes;
  int hours;
  int days;
  int years;

  SecondsToString(int seconds, BuildContext context) {
    _allSeconds = seconds;
    _context = context;
    _buildSeperatedValues();
  }

  SecondsToString.fromMiliseconds(int miliseconds, BuildContext context) {
    _allSeconds = (miliseconds / 1000).floor();
    _context = context;
    _buildSeperatedValues();
  }

  void _buildSeperatedValues() {
    int _allMinutes = (_allSeconds / 60).floor();
    int _allHours = (_allMinutes / 60).floor();
    int _allDays = (_allHours / 24).floor();

    years = (_allDays / 365).floor();

    seconds = _allSeconds - (_allMinutes * 60);
    minutes = _allMinutes - (_allHours * 60);
    hours = _allHours - (_allDays * 24);
    days = _allDays - (years * 365);
  }

  String withZero() {
    return "$years ${AppLocalizations.of(_context).secondsToStringPageYearsUnit}  $days ${AppLocalizations.of(_context).secondsToStringPageDaysUnit}  $hours ${AppLocalizations.of(_context).secondsToStringPageHoursUnit}  $minutes ${AppLocalizations.of(_context).secondsToStringPageMinutesUnit}  $seconds ${AppLocalizations.of(_context).secondsToStringPageSecondsUnit}";
  }

  String withoutZero(){
    return (years != 0 ? years.toString() + " " + AppLocalizations.of(_context).secondsToStringPageYearsUnit + "  ": "") +
      (days != 0 ? days.toString() + " " + AppLocalizations.of(_context).secondsToStringPageDaysUnit + "  ": "") +
      (hours != 0? hours.toString() + " " + AppLocalizations.of(_context).secondsToStringPageHoursUnit + "  " : "") +
      (minutes != 0 ? minutes.toString() + " " + AppLocalizations.of(_context).secondsToStringPageMinutesUnit + "  ": "") +
      (seconds != 0 ? seconds.toString() + " " + AppLocalizations.of(_context).secondsToStringPageSecondsUnit + "  ": "");
  }
}
