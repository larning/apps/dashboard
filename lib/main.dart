import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' as Foundation;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:igplaner_mobile/controllers/deviceParams/screenTypeController.dart';
import 'package:igplaner_mobile/controllers/network/mapsController.dart';
import 'package:igplaner_mobile/controllers/scaffold/appMaterialApp.dart';
import 'package:igplaner_mobile/pages/wifi/wifiDetailsPage.dart';

import './localization/localizations.dart';
import './controllers/page_transitions/slideTransitionRoute.dart';

import './controllers/drawer/drawer.dart';
import './controllers/users/userDataController.dart';

import './pages/login.dart';
import './pages/document_camera.dart';
import './pages/wifi.dart';
import './pages/home.dart';
import 'package:camera/camera.dart';
import './controllers/network/urlController.dart';

List<CameraDescription> cameras;
UserDataController userController = UserDataController();

Future<void> main() async {
  runApp(MyApp());
  cameras = await availableCameras();
}

class SettingsProvider extends StatelessWidget {
  final Widget child;
  SettingsProvider({@required this.child});

  @override
  Widget build(BuildContext ctx) {
    return MapsProvider(
      mapsController:
          MapsController(apiKey: "eff99ad7-ccb9-48a8-a0bf-55d46a2c2402"),
      child: UrlProvider(
        url: "https://igplaner.home-webserver.de:3001/",
        child: UserDataProvider(controller: userController, child: child),
      ),
    );
  }
}

final ThemeData appTheme = ThemeData(
    accentColor: Colors.purple,
    primarySwatch: Colors.deepOrange,
    brightness: Brightness.light);

final ThemeData darkAppTheme = ThemeData(
  brightness: Brightness.dark,
  accentColor: Colors.purple,
);

class MyApp extends StatelessWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SettingsProvider(
      child: AppMaterialApp(
        earlyBuilder: (BuildContext context, Widget child, _) =>
            ScreenTypeProvider(
          context: context,
          child: DrawerProvider(child: child),
        ),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate
        ],
        supportedLocales: [Locale("en"), Locale("de")],
        onGenerateTitle: (BuildContext context) =>
            AppLocalizations.of(context).appTitle,
        initialRoute:
            Foundation.kReleaseMode ? LoginPage.routeName : HomePage.routeName,
        routes: {
          HomePage.routeName: (BuildContext context) => HomePage(),
          WifiPage.routeName: (BuildContext context) => WifiPage(),
          LoginPage.routeName: (BuildContext context) => LoginPage(
                cameras: cameras,
              ),
          WifiDetailsPage.routeName: (BuildContext context) => WifiDetailsPage()
        },
        onGenerateRoute: (RouteSettings route) {
          final String routeTopPointer = route.name.split("/")[1];
          final String routeArgs = route.name.split("/").length > 2
              ? route.name.split("/")[2]
              : null;

          switch ("/" + routeTopPointer) {
            case "/login_slide":
              return SlideRoute(
                  direction: routeArgs == "up" || routeArgs == "top"
                      ? SlideDirection.up
                      : routeArgs == "down" || routeArgs == "bottom"
                          ? SlideDirection.down
                          : routeArgs == "right"
                              ? SlideDirection.right
                              : SlideDirection.left,
                  page: LoginPage(cameras: cameras));
              break;
            case "/login_camera":
              return MaterialPageRoute(
                  builder: (BuildContext ctx) => DocumentCameraPage(
                      args: route.arguments, cameras: cameras));
          }
          return null;
        },
        theme: appTheme,
        darkTheme: darkAppTheme,
      ),
    );
  }
}

class DrawerProvider extends StatelessWidget {
  final Widget child;
  DrawerProvider({@required this.child});

  @override
  Widget build(BuildContext context) {
    return CustomDrawerProvider(
      child: child,
      controller: (() {
        LoginDrawerEntry _loginEntry = LoginDrawerEntry.bordered(
            buildContext: context,
            loggedInIcon: Icons.exit_to_app,
            loggedOutIcon: Icons.supervised_user_circle,
            loggedInText: "Abmelden",
            loggedOutText: "Anmelden",
            logInNamedRoute: "/login_slide/left",
            onLogoutTap: (BuildContext context) {});

        return CustomDrawerController(
            header: CustomHeader(
                type: CustomHeaderType.user,
                backgroundImage:
                    AssetImage("assets/images/bookshelf_background.jpg")),
            mainContent: <DrawerEntry>[
              _loginEntry.loginDependant(context),
              DrawerEntry.bordered(
                icon: Icons.ac_unit,
                namedRoute: HomePage.routeName,
                text: AppLocalizations.of(context)
                    .substitutionSchedulePageSubstitutionSchedule,
              ),
              // UserControlledDrawerEntry.bordered(
              //     namedRoute: WifiPage.routeName,
              //     icon: Icons.wifi,
              //     text: AppLocalizations.of(context).wlanPagePageTitle,
              //     context: context,
              //     permissionRequired: ["teacher", "admin", "dev"]),
              DrawerEntry.bordered(
                  namedRoute: WifiPage.routeName,
                  icon: Icons.wifi,
                  text: AppLocalizations.of(context).wlanPagePageTitle)
            ],
            bottomContent: <DrawerEntry>[
              _loginEntry.logOutDependant(context),
            ]);
      })(),
    );
  }
}
