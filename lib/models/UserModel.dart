import 'package:flutter/material.dart';

class User {
  final UserId id;
  final String userEmail;
  final String userName;
  CircleAvatar userImage;
  List<String> permissions;

  User(
      {@required this.id,
      @required this.userEmail,
      @required this.userName,
      CircleAvatar userAvatar,
      this.permissions}) {
    if (userAvatar != null)
      userImage = userAvatar;
    else {
      userImage = CircleAvatar(
          child: Container(
              child: Align(
        alignment: Alignment.center,
        child: Text(userName
            .split(" ")
            .map((String part) => part[0])
            .reduce((String a, String b) => a + b)),
      )));
    }
  }
}

class UserId {
  final String id;
  UserId(this.id);
}
