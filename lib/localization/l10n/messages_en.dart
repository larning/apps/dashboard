// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appTitle" : MessageLookupByLibrary.simpleMessage("IGPlaner Mobile Application"),
    "homePageHomePageTitle" : MessageLookupByLibrary.simpleMessage("Overview"),
    "loginPageA4DocumentButtonText" : MessageLookupByLibrary.simpleMessage("A4-Document"),
    "loginPageA4DocumentPictureHelperText" : MessageLookupByLibrary.simpleMessage("Take an picture of your document."),
    "loginPageCheckedImagesProceed" : MessageLookupByLibrary.simpleMessage("Checked images, proceed"),
    "loginPageDocumentSelectionHeader" : MessageLookupByLibrary.simpleMessage("Select Document type"),
    "loginPageDocumentSeletionText" : MessageLookupByLibrary.simpleMessage("To proceed, you have to upload an Document that proofes your School. This is nessesary because the Laws don\'t allow to tell other people Informations that do belong to other people. We want to make sure everybody respects the law. To proofe it, you need to upload an official document from your school with the name of your school, an date, your class, and your own name. At the moment, your Student card, or an A4-Document are accepted."),
    "loginPageDoneSignupHeader" : MessageLookupByLibrary.simpleMessage("You are ready to send your Signup request!"),
    "loginPageDoneSignupText" : MessageLookupByLibrary.simpleMessage("You are ready to send your signup request! After your request is send, we\'ll try to process it as fast as possible. You will get an email if your request is rejected or accepted."),
    "loginPageEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "loginPageEmailUsername" : MessageLookupByLibrary.simpleMessage("Email / Username"),
    "loginPageErrorLogin" : MessageLookupByLibrary.simpleMessage("Wrong username or password"),
    "loginPageInnerPictureOfStudentIdentityCardTitle" : MessageLookupByLibrary.simpleMessage("Inner of your Student identity card"),
    "loginPageLogin" : MessageLookupByLibrary.simpleMessage("Login"),
    "loginPageOuterPictureOfStudentIdentityCardTitle" : MessageLookupByLibrary.simpleMessage("Outer of your student identity card"),
    "loginPagePassword" : MessageLookupByLibrary.simpleMessage("Password"),
    "loginPagePictureOfDocument" : MessageLookupByLibrary.simpleMessage("Your document"),
    "loginPageProceed" : MessageLookupByLibrary.simpleMessage("Proceed"),
    "loginPageRealName" : MessageLookupByLibrary.simpleMessage("Real Name (\'Max Mustermann\')"),
    "loginPageRedoImage" : MessageLookupByLibrary.simpleMessage("Redo Image"),
    "loginPageRetypePassword" : MessageLookupByLibrary.simpleMessage("Retype Password"),
    "loginPageSendSignupRequestButtonText" : MessageLookupByLibrary.simpleMessage("Send Signup request"),
    "loginPageSignUp" : MessageLookupByLibrary.simpleMessage("Sign Up"),
    "loginPageSignupButtonText" : MessageLookupByLibrary.simpleMessage("Send signup request"),
    "loginPageSignupUnscucessfulSnackbar" : MessageLookupByLibrary.simpleMessage("Error while signing up - maybe Email already registered"),
    "loginPageStudentIdentityCardButtonText" : MessageLookupByLibrary.simpleMessage("Student identity card"),
    "loginPageStudentIdentityCardInnerPictureHelperText" : MessageLookupByLibrary.simpleMessage("Take an picture of the inner of your Student identity card."),
    "loginPageStudentIdentityCardOuterPictureHelperText" : MessageLookupByLibrary.simpleMessage("Take an image of the outer of your student identity card."),
    "loginPageSwitchToLogin" : MessageLookupByLibrary.simpleMessage("Switch to Login"),
    "loginPageSwitchToSignup" : MessageLookupByLibrary.simpleMessage("Switch to Signup"),
    "loginPageTakeNextPicture" : MessageLookupByLibrary.simpleMessage("Take next picture"),
    "loginPageUsername" : MessageLookupByLibrary.simpleMessage("Username"),
    "loginPageWaitingForImagePlaceholder" : MessageLookupByLibrary.simpleMessage("Waiting for image ..."),
    "secondsToStringPageDaysUnit" : MessageLookupByLibrary.simpleMessage("days"),
    "secondsToStringPageHoursUnit" : MessageLookupByLibrary.simpleMessage("hours"),
    "secondsToStringPageMinutesUnit" : MessageLookupByLibrary.simpleMessage("minutes"),
    "secondsToStringPageSecondsUnit" : MessageLookupByLibrary.simpleMessage("seconds"),
    "secondsToStringPageYearsUnit" : MessageLookupByLibrary.simpleMessage("years"),
    "substitutionSchedulePageSubstitutionSchedule" : MessageLookupByLibrary.simpleMessage("Substitution schedule"),
    "termsAndConditionsSwitchListTilePageDoYouAccept" : MessageLookupByLibrary.simpleMessage("Do you accept our"),
    "termsAndConditionsSwitchListTilePageTermsAndConditions" : MessageLookupByLibrary.simpleMessage("Terms & Conditions"),
    "termsAndConditionsSwitchListTilePageTextAfterTermsAndConditions" : MessageLookupByLibrary.simpleMessage("?"),
    "termsAndConditionsSwitchListTilePageYouHaveToAccepttermsAndConditionstext" : MessageLookupByLibrary.simpleMessage("You have to accept our terms and conditions!"),
    "wlanPageActiveText" : MessageLookupByLibrary.simpleMessage("active"),
    "wlanPageAlreadyConnected" : MessageLookupByLibrary.simpleMessage("Already connected"),
    "wlanPageConnectNowButtontext" : MessageLookupByLibrary.simpleMessage("Connect now"),
    "wlanPageExpiredBecause" : MessageLookupByLibrary.simpleMessage("Expired because"),
    "wlanPageExpiredText" : MessageLookupByLibrary.simpleMessage("disabled"),
    "wlanPageExpires" : MessageLookupByLibrary.simpleMessage("Expires in"),
    "wlanPagePageTitle" : MessageLookupByLibrary.simpleMessage("Wifi"),
    "wlanPagePassword" : MessageLookupByLibrary.simpleMessage("Password"),
    "wlanPageStatusTitle" : MessageLookupByLibrary.simpleMessage("Status"),
    "wlanPageUsername" : MessageLookupByLibrary.simpleMessage("Username"),
    "wlanPageWifiNameSSID" : MessageLookupByLibrary.simpleMessage("Wifi-Name (SSID)"),
    "wlanPageYourDataTitle" : MessageLookupByLibrary.simpleMessage("Your Wifi data")
  };
}
