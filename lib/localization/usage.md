## Commands

generate localizations based on localizations.dart:

`flutter pub pub run intl_translation:extract_to_arb --output-dir=lib/localization/l10n lib/localization/localizations.dart lib/localization/**/*.dart`

generate dart files based on the arb files:

`flutter pub pub run intl_translation:generate_from_arb --output-dir=lib/localization/l10n --no-use-deferred-loading lib/localization/**/*.dart lib/localization/localizations.dart lib/localization/l10n/intl_*.arb`
