import 'package:intl/intl.dart';

mixin HomePage {
  //Insert here...

  String get homePageHomePageTitle => Intl.message("Overview",
      name: "homePageHomePageTitle",
      desc: "Text displayed as title of the home (overview) Page");
}
