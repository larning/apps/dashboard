import 'package:intl/intl.dart';

mixin SubstitutionSchedulePage {
  String get substitutionSchedulePageSubstitutionSchedule =>
      Intl.message("Substitution schedule",
          name: "substitutionSchedulePageSubstitutionSchedule",
          desc: "Subtitution Schedule drawer entry");
}
