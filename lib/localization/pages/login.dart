import 'package:intl/intl.dart';

mixin LoginPage {
  String get loginPageSignUp => Intl.message("Sign Up",
      name: "loginPageSignUp", desc: "Sign up button text");

  String get loginPageLogin =>
      Intl.message("Login", name: "loginPageLogin", desc: "Login page name");

  String get loginPagePassword => Intl.message("Password",
      name: "loginPagePassword", desc: "Password label for text Input");

  String get loginPageEmailUsername => Intl.message("Email / Username",
      name: "loginPageEmailUsername", desc: "Email / Username label");

  String get loginPageSwitchToSignup => Intl.message("Switch to Signup",
      name: "loginPageSwitchToSignup",
      desc: "Switch to Signup button on login Screen");

  String get loginPageSwitchToLogin => Intl.message("Switch to Login",
      name: "loginPageSwitchToLogin",
      desc: "Switch to Login field on signup page");

  String get loginPageUsername => Intl.message("Username",
      name: "loginPageUsername", desc: "Username field on signup page");

  String get loginPageEmail => Intl.message("Email",
      name: "loginPageEmail", desc: "Email field on Signup page");

  String get loginPageRetypePassword => Intl.message("Retype Password",
      name: "loginPageRetypePassword",
      desc:
          "Retype password field on the signup page for comparision with the first Password field");

  String get loginPageSignupButtonText => Intl.message("Send signup request",
      name: "loginPageSignupButtonText",
      desc: "Button who submittes the signup request");

  String get loginPageProceed => Intl.message("Proceed",
      name: "loginPageProceed",
      desc:
          "Proceed Button on sinup Page (Next Page: Take Image of an official Document)");

  String get loginPageRealName => Intl.message("Real Name ('Max Mustermann')",
      name: "loginPageRealName",
      desc: "Real name field on signup page (must contain space)");

  String get loginPageDocumentSelectionHeader => Intl.message(
      "Select Document type",
      name: "loginPageDocumentSelectionHeader",
      desc:
          "The header for the text who tells the user why to upload an Document");

  String get loginPageDocumentSeletionText => Intl.message(
      "To proceed, you have to upload an Document that proofes your School. This is nessesary because the Laws don't allow to tell other people Informations that do belong to other people. We want to make sure everybody respects the law. To proofe it, you need to upload an official document from your school with the name of your school, an date, your class, and your own name. At the moment, your Student card, or an A4-Document are accepted.",
      name: "loginPageDocumentSeletionText",
      desc:
          "text that tells the user why we need that information and what documents he can use.");

  String get loginPageA4DocumentButtonText => Intl.message("A4-Document",
      name: "loginPageA4DocumentButtonText",
      desc: "The text for the button to select an A4 Document");

  String get loginPageStudentIdentityCardButtonText =>
      Intl.message("Student identity card",
          name: "loginPageStudentIdentityCardButtonText",
          desc: "The button to select an Student identity card");

  String get loginPageWaitingForImagePlaceholder => Intl.message(
      "Waiting for image ...",
      name: "loginPageWaitingForImagePlaceholder",
      desc:
          "The text displayed while waiting for the cameera to return the taken Image");

  String get loginPageStudentIdentityCardOuterPictureHelperText => Intl.message(
      "Take an image of the outer of your student identity card.",
      name: "loginPageStudentIdentityCardOuterPictureHelperText",
      desc:
          "The helper text displayed if the user should take an image of the outer of his/her Student identity card.");

  String get loginPageA4DocumentPictureHelperText => Intl.message(
      "Take an picture of your document.",
      name: "loginPageA4DocumentPictureHelperText",
      desc:
          "The text displayed if the user should make an picture of his/her official document.");

  String get loginPageOuterPictureOfStudentIdentityCardTitle => Intl.message(
      "Outer of your student identity card",
      name: "loginPageOuterPictureOfStudentIdentityCardTitle",
      desc:
          "The text displayed as titile in the expansiontile for the outer of the Student identity card");

  String get loginPageRedoImage => Intl.message("Redo Image",
      name: "loginPageRedoImage",
      desc: "Button to press to redo the image taken of the document.");

  String get loginPageStudentIdentityCardInnerPictureHelperText => Intl.message(
      "Take an picture of the inner of your Student identity card.",
      name: "loginPageStudentIdentityCardInnerPictureHelperText",
      desc:
          "Text displayed While taking an picture of the inner of the student identity card");

  String get loginPageTakeNextPicture => Intl.message("Take next picture",
      name: "loginPageTakeNextPicture",
      desc:
          "The text for the button that should be pressed to take the next picture");

  String get loginPageInnerPictureOfStudentIdentityCardTitle => Intl.message(
      "Inner of your Student identity card",
      name: "loginPageInnerPictureOfStudentIdentityCardTitle",
      desc:
          "Text displayed as title for expansionPanel for the inner Student Identity card Picture");

  String get loginPagePictureOfDocument => Intl.message("Your document",
      name: "loginPagePictureOfDocument",
      desc:
          "Text displayed as title of the expansion panel displaying the Picture taken of the user's document");

  String get loginPageCheckedImagesProceed => Intl.message(
      "Checked images, proceed",
      name: "loginPageCheckedImagesProceed",
      desc:
          "Button text for the 'i have checked the images, please proceed' button.");

  String get loginPageDoneSignupHeader => Intl.message(
      "You are ready to send your Signup request!",
      name: "loginPageDoneSignupHeader",
      desc:
          "Header for the text displayed after signup process is complete and the user is able to send her/his request.");

  String get loginPageDoneSignupText => Intl.message(
      "You are ready to send your signup request! After your request is send, we'll try to process it as fast as possible. You will get an email if your request is rejected or accepted.",
      name: "loginPageDoneSignupText",
      desc:
          "Text dispayed below the header after Signup Completed. User have to send the Signup Request using the button below.");

  String get loginPageSendSignupRequestButtonText =>
      Intl.message("Send Signup request",
          name: "loginPageSendSignupRequestButtonText",
          desc: "Text for the button that sends the signup request");

  String get loginPageErrorLogin => Intl.message("Wrong username or password",
      name: "loginPageErrorLogin", desc: "Text displayed if login fails");

  String get loginPageSignupUnscucessfulSnackbar =>
      Intl.message("Error while signing up - maybe Email already registered",
          name: "loginPageSignupUnscucessfulSnackbar",
          desc: "Text displaed if signup fails");
}
