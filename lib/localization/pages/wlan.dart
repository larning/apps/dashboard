import 'package:intl/intl.dart';

mixin WlanPage {
  // Placeholder comment

  String get wlanPageYourDataTitle => Intl.message("Your Wifi data",
      name: "wlanPageYourDataTitle",
      desc: "Text displayed as header for the 'Your Wifi data' Card");

  String get wlanPageWifiNameSSID => Intl.message("Wifi-Name (SSID)",
      name: "wlanPageWifiNameSSID",
      desc: "Text displayed as header above the SSID");

  String get wlanPageUsername => Intl.message("Username",
      name: "wlanPageUsername",
      desc: "Text displayed above the users wifi username (Auto-gnerated)");

  String get wlanPagePassword => Intl.message("Password",
      name: "wlanPagePassword",
      desc: "text displaed above the users Wifi password");

  String get wlanPageStatusTitle => Intl.message("Status",
      name: "wlanPageStatusTitle",
      desc:
          "Text displayed above the status of the users Wifi Account (Title)");

  String get wlanPageExpires => Intl.message("Expires in",
      name: "wlanPageExpires",
      desc: "Text displayed above the expires in field");

  String get wlanPageAlreadyConnected => Intl.message("Already connected",
      name: "wlanPageAlreadyConnected",
      desc:
          "Text displayed beneeth the button if you are already onnected to the wifi network");

  String get wlanPageConnectNowButtontext => Intl.message("Connect now",
      name: "wlanPageConnectNowButtontext",
      desc:
          "Text displayed at the Button who connects the user to the wifi network");

  String get wlanPageExpiredBecause => Intl.message("Expired because",
      name: "wlanPageExpiredBecause",
      desc: "Text displayed as header of the expired because field");

  String get wlanPageExpiredText => Intl.message("disabled",
      name: "wlanPageExpiredText",
      desc: "Text displayed if the Wifi account is disabled");

  String get wlanPageActiveText => Intl.message("active",
      name: "wlanPageActiveText",
      desc: "Text displayed if the wifi account of the user is active");

  String get wlanPagePageTitle => Intl.message("Wifi",
      name: "wlanPagePageTitle",
      desc:
          "Text displayed as title of the page (in the menu as well as at the top of the page)");
}
