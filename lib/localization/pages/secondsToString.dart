import 'package:intl/intl.dart';

mixin SecondsToString {
  //Insert here...

  String get secondsToStringPageYearsUnit => Intl.message("years",
      name: "secondsToStringPageYearsUnit",
      desc: "Text displayed as unit for one or multiple years");

  String get secondsToStringPageDaysUnit => Intl.message("days",
      name: "secondsToStringPageDaysUnit", desc: "Unit for one or more days");

  String get secondsToStringPageHoursUnit => Intl.message("hours",
      name: "secondsToStringPageHoursUnit", desc: "Unit for one or more hours");

  String get secondsToStringPageMinutesUnit => Intl.message("minutes",
      name: "secondsToStringPageMinutesUnit",
      desc: "Unit for one or more minutes");

  String get secondsToStringPageSecondsUnit => Intl.message("seconds",
      name: "secondsToStringPageSecondsUnit",
      desc: "Unit for one or more Seconds");
}
