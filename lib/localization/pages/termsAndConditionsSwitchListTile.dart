import 'package:intl/intl.dart';

mixin TermsAndConditionsSwitchListTile {
  //test

  String get termsAndConditionsSwitchListTilePageDoYouAccept =>
      Intl.message("Do you accept our",
          name: "termsAndConditionsSwitchListTilePageDoYouAccept",
          desc: "Text before link to terms of conditions");

  String get termsAndConditionsSwitchListTilePageTermsAndConditions =>
      Intl.message("Terms & Conditions",
          name: "termsAndConditionsSwitchListTilePageTermsAndConditions",
          desc: "Link to terms of conditions after Text ->'DoYouAccept'");

  String get termsAndConditionsSwitchListTilePageTextAfterTermsAndConditions =>
      Intl.message("?",
          name:
              "termsAndConditionsSwitchListTilePageTextAfterTermsAndConditions",
          desc: "Text displayed after terms and conditions. Usually an '?'");

  String get termsAndConditionsSwitchListTilePageYouHaveToAccepttermsAndConditionstext =>
      Intl.message("You have to accept our terms and conditions!",
          name:
              "termsAndConditionsSwitchListTilePageYouHaveToAccepttermsAndConditionstext",
          desc:
              "Text displayed if the user didn't accepted the terms and conditions.");
}
