import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'l10n/messages_all.dart';

import './pages/login.dart' show LoginPage;
import './pages/substitutionSchedule.dart' show SubstitutionSchedulePage;
import './pages/termsAndConditionsSwitchListTile.dart'
    show TermsAndConditionsSwitchListTile;
import './pages/wlan.dart' show WlanPage;
import './pages/home.dart' show HomePage;
import './pages/secondsToString.dart' show SecondsToString;

const supportedLanguageCodes = ["en", "de"];

class AppLocalizations
    with
        LoginPage,
        SubstitutionSchedulePage,
        TermsAndConditionsSwitchListTile,
        WlanPage,
        HomePage,
        SecondsToString {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get appTitle {
    return Intl.message("IGPlaner Mobile Application",
        name: "appTitle", desc: "The application title.");
  }

  static _AppLocalizationsDelegate delegate = new _AppLocalizationsDelegate();
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return supportedLanguageCodes.contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    return false;
  }
}
