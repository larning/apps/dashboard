import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../localization/localizations.dart';

class TermsOfServiceSwitchListTile extends StatelessWidget {
  const TermsOfServiceSwitchListTile(
      {this.padding = const EdgeInsets.all(8.0),
      @required this.value,
      @required this.onChanged,
      @required this.termsAndConditionsLink,
      this.switchColor});

  final EdgeInsets padding;
  final bool value;
  final Function onChanged;
  final String termsAndConditionsLink;
  final Color switchColor;

  void _launchUrl() async {
    if (await canLaunch(termsAndConditionsLink)) {
      await launch(termsAndConditionsLink);
    } else {
      throw "Could not launch $termsAndConditionsLink";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Row(
        children: <Widget>[
          Expanded(
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: AppLocalizations.of(context)
                              .termsAndConditionsSwitchListTilePageDoYouAccept
                              .trim() +
                          " "),
                  TextSpan(
                    text: AppLocalizations.of(context)
                        .termsAndConditionsSwitchListTilePageTermsAndConditions
                        .trim(),
                    style: TextStyle(
                      color: Colors.blueAccent,
                      decoration: TextDecoration.underline,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        _launchUrl();
                      },
                  ),
                  TextSpan(
                      text: " " +
                          AppLocalizations.of(context)
                              .termsAndConditionsSwitchListTilePageTextAfterTermsAndConditions
                              .trim())
                ],
              ),
            ),
          ),
          Switch(
            value: value,
            onChanged: (bool newValue) {
              onChanged(newValue);
            },
            activeColor: switchColor,
          ),
        ],
      ),
    );
  }
}
