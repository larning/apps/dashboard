import 'package:flutter/material.dart';

class BorderedListTile extends StatelessWidget {
  final BorderRadius borderRadius;
  final String title;
  final IconData leading;
  final Function onTap;

  BorderedListTile(
      {@required this.borderRadius,
      @required this.title,
      this.leading,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      borderRadius: this.borderRadius,
      child: Row(
        children: <Widget>[
          this.leading != null
              ? SizedBox(
                  height: 56,
                  width: 56,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: SizedBox(
                          width: 24,
                          height: 24,
                          child: Icon(
                            this.leading,
                            color: Colors.grey.shade600,
                          )),
                    ),
                  ),
                )
              : Container(),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                this.title,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14.2,
                    letterSpacing: 0.1),
              ),
            ),
          )
        ],
      ),
    );
  }
}
