import 'package:flutter/material.dart';

class FlexibleHeightGrid extends StatelessWidget {
  final bool shrinkWrap;
  final List<Widget> children;
  final double maxCrossAxisExtend;
  final int crossAxisCount;

  FlexibleHeightGrid(
      {Key key,
      this.shrinkWrap = false,
      @required this.children,
      this.maxCrossAxisExtend,
      this.crossAxisCount})
      : super(key: key);

  Widget _buildGrid(BuildContext context, BoxConstraints constraints) => Row(
        children: _buildColumns(context, constraints.maxWidth),
      );

  List<Widget> _buildColumns(BuildContext context, double width) {
    if (maxCrossAxisExtend == null && crossAxisCount == null)
      throw new ExtendOrCountRequiredException(
          "Neither count or extend provided");

    int count = this.crossAxisCount != null
        ? this.crossAxisCount
        : (width ~/ this.maxCrossAxisExtend + 1);

    final List<List<Widget>> columns = [];
    for (var i = 0; i < count; i++) {
      columns.add([]);
    }

    int insertTo = 0;
    this.children.forEach((element) {
      if (insertTo == count) insertTo = 0;
      columns[insertTo].add(element);
      insertTo++;
    });

    return columns
        .map((list) => Flexible(
              flex: 1,
              child: Column(children: list),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (_, constraints) => shrinkWrap
            ? Container(child: _buildGrid(context, constraints))
            : SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: _buildGrid(context, constraints),
              ));
  }
}

class ExtendOrCountRequiredException implements Exception {
  String cause;
  ExtendOrCountRequiredException(this.cause);
}
