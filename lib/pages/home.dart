import 'package:flutter/material.dart';
import 'package:igplaner_mobile/controllers/scaffold/appScaffold.dart';
import '../localization/localizations.dart';

class HomePage extends StatelessWidget {
  static const routeName = "/";

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      displayDrawer: true,
      appBar: (context) => AppBar(
        title: Text(AppLocalizations.of(context).homePageHomePageTitle),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Recent Messages",
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.access_alarm),
                    title: Text("TestMessage"),
                    subtitle: Text("Test Description"),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
