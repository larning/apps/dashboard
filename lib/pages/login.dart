import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/rendering.dart';
import 'package:igplaner_mobile/controllers/scaffold/appScaffold.dart';
import 'package:igplaner_mobile/pages/home.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:flutter/scheduler.dart';

import '../localization/localizations.dart';
import './document_camera.dart' as documentCameraPage;
import '../widgets/TermsAndConditionsSwitchListTile.dart';
import '../controllers/login/loginDataController.dart';

bool onlyOnceOpen = true;
List<String> picturesTaken = [null, null];
int step = 1;
double aspectRatio;

class LoginPage extends StatefulWidget {
  static const routeName = "/login";
  final List<CameraDescription> cameras;

  LoginPage({@required this.cameras});
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  _LoginPageMode oldMode = _LoginPageMode.none;
  _LoginPageMode mode = _LoginPageMode.none;

  AnimationController _controller;
  AnimationController _signupCompleteButtonAnimationController;
  AnimationController _signupDisplayFinishedPage;
  AnimationController _dismissController;

  _DocumentType _lastDocumentType = _DocumentType.unset;
  CameraController controller;

  bool _termsAccepted = false;
  OverlayEntry _overlayEntry;
  OverlayEntry _textOverlay;
  OverlayEntry _dismissAnimation;
  LayerLink _link = LayerLink();
  double _buttonWidth;
  bool _switchToLoginAnimationRunning = false;

  FocusNode _loginPasswordFocusNode = FocusNode();
  final _loginFormKey = GlobalKey<FormState>();

  Map<String, String> _loginFormValues = {"username": null, "passkkword": null};
  bool _loginFormSaving = false;

  Map<String, FocusNode> _signupFormFocusNodes = {
    "username": FocusNode(),
    "realname": FocusNode(),
    "password": FocusNode(),
    "retypepassword": FocusNode()
  };
  final _signupFormKey = GlobalKey<FormState>();
  Map<String, String> _signupFormValues = {
    "email": null,
    "username": null,
    "realname": null,
    "password": null,
    "retypepassword": null
  };

  TextEditingController _signupPasswordController = TextEditingController();

  AnimationStatusListener _signupCompleteListener;
  bool shownErrorMessage = false;

  @override
  void initState() {
    super.initState();
    // controller = CameraController(widget.cameras[0], ResolutionPreset.low);
    // controller.initialize().then((_) {
    //   if (!mounted) {
    //     return;
    //   }
    //   setState(() {});
    // });
    _signupCompleteButtonAnimationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    _signupCompleteButtonAnimationController.addListener(() {
      Overlay.of(context).setState(() {});
    });
    _signupDisplayFinishedPage =
        AnimationController(duration: Duration(milliseconds: 600), vsync: this);
    _signupDisplayFinishedPage.addListener(() {
      Overlay.of(context).setState(() {});
    });
    _dismissController =
        AnimationController(duration: Duration(milliseconds: 600), vsync: this);
    _dismissController.addListener(() {
      Overlay.of(context).setState(() {});
    });
    _signupDisplayFinishedPage.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        this._textOverlay = _createSignupDoneScreenButtonsOverlay(context);
        Overlay.of(context).insert(this._textOverlay);
      }
    });
    _dismissController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        this._dismissAnimation.remove();
        _termsAccepted = false;
        _switchToLoginAnimationRunning = false;
        _signupCompleteButtonAnimationController.reset();
        _signupDisplayFinishedPage.reset();
        _dismissController.reset();
        _overlayEntry = null;
        _textOverlay = null;
        _dismissAnimation = null;
      }
    });

    _changeWidget(_LoginPageMode.login);
  }

  @override
  @mustCallSuper
  void dispose() {
    _signupCompleteButtonAnimationController
        .removeStatusListener(_signupCompleteListener);
    super.dispose();
  }

  OverlayEntry _createSignupDoneScreenButtonsOverlay(BuildContext context) =>
      OverlayEntry(
        builder: (BuildContext context) {
          return Positioned(
            top: 64,
            left: 32,
            child: Container(
              width: MediaQuery.of(context).size.width - 64,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Header Todo:Remove placeholder", //TODO: Remove placeholder @low
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .merge(TextStyle(color: Colors.white)),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Text (Lorem ipsum dolor sit amet ...) Todo:Remove placeholder", //TODO: Remove placeholder @low
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .merge(TextStyle(color: Colors.white)),
                  ),
                  ButtonBar(
                    children: <Widget>[
                      RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          "Login Todo:Remove placeholder", //TODO: Remove placeholder @low
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          _textOverlay.remove();
                          _overlayEntry.remove();
                          _changeWidget(_LoginPageMode.login);
                          _dismissAnimation =
                              OverlayEntry(builder: (BuildContext context) {
                            return Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Theme.of(context).accentColor),
                                width: Tween<double>(
                                        begin:
                                            MediaQuery.of(context).size.width *
                                                1.5,
                                        end: 0)
                                    .animate(_dismissController)
                                    .value,
                                height: Tween<double>(
                                        begin:
                                            MediaQuery.of(context).size.height *
                                                1.5,
                                        end: 0)
                                    .animate(_dismissController)
                                    .value,
                              ),
                            );
                          });
                          Overlay.of(context).insert(_dismissAnimation);
                          _dismissController.forward();
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        },
      );

  void _changeWidget(_LoginPageMode newMode) {
    oldMode = mode;
    mode = newMode;
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 400));
    _controller.addListener(() {
      setState(() {});
    });
    _controller.forward();
  }

  void _saveLoginForm(BuildContext context) {
    if (_loginFormKey.currentState.validate() == true) {
      _loginFormKey.currentState.save();
      LoginDataController.login(
              _loginFormValues["username"], _loginFormValues["password"],
              context: context)
          .then((bool success) {
        if (!success) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(
                AppLocalizations.of(context).loginPageErrorLogin,
              ),
              backgroundColor: Colors.red,
            ),
          );
          setState(() {
            _loginFormSaving = false;
          });
        } else {
          Navigator.pushReplacementNamed(context, HomePage.routeName);
        }
      });
    }
  }

  void _saveSignupForm(BuildContext context) {
    if (!_signupFormKey.currentState.validate()) return;
    _signupFormKey.currentState.save();
    _changeWidget(_LoginPageMode.documentSelection);
  }

  Widget _buildLoginForm(BuildContext context) {
    return Form(
      key: _loginFormKey,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: TextFormField(
              validator: LoginDataController.emailUsernameValidator,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) {
                FocusScope.of(context).requestFocus(_loginPasswordFocusNode);
              },
              onSaved: (String usernameOrEmail) {
                _loginFormValues["username"] = usernameOrEmail;
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPageEmailUsername,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 16.0),
            child: TextFormField(
              obscureText: true,
              autocorrect: false,
              keyboardType: TextInputType.text,
              validator: LoginDataController.passwordValidator,
              focusNode: _loginPasswordFocusNode,
              textInputAction: TextInputAction.send,
              onFieldSubmitted: (_) {
                _saveLoginForm(context);
              },
              onSaved: (String password) {
                _loginFormValues["password"] = password;
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPagePassword,
              ),
            ),
          ),
          ButtonBar(
            children: <Widget>[
              _loginFormSaving
                  ? Container()
                  : FlatButton(
                      child: Text(
                        AppLocalizations.of(context).loginPageSwitchToSignup,
                        style: TextStyle(color: Colors.black),
                      ),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                  title:
                                      Text("Do you really want to continue?"),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                          "Signup is not implemented now. Do you want to continue anyway?"),
                                      Wrap(
                                        children: <Widget>[
                                          FlatButton(
                                            child: Text("Continue anyway"),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              _changeWidget(
                                                  _LoginPageMode.signup);
                                            },
                                          ),
                                          SizedBox(
                                            width: 8,
                                          ),
                                          RaisedButton(
                                            color: Colors.redAccent,
                                            child: Text(
                                              "Abort",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ));
                      },
                    ),
              RaisedButton(
                color: Theme.of(context).primaryColor.withOpacity(0.8),
                textColor: Colors.white,
                child: _loginFormSaving
                    ? CircularProgressIndicator()
                    : Text(AppLocalizations.of(context).loginPageLogin),
                onPressed: () {
                  if (!_loginFormSaving) _saveLoginForm(context);
                },
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildSignupForm(BuildContext context) {
    return Form(
      key: _signupFormKey,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) {
                FocusScope.of(context)
                    .requestFocus(_signupFormFocusNodes["username"]);
              },
              onSaved: (String email) {
                _signupFormValues["email"] = email;
              },
              validator: LoginDataController.emailValidator,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPageEmail,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 16.0),
            child: TextFormField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) {
                FocusScope.of(context)
                    .requestFocus(_signupFormFocusNodes["realname"]);
              },
              onSaved: (String username) {
                _signupFormValues["username"] = username;
              },
              focusNode: _signupFormFocusNodes["username"],
              validator: LoginDataController.usernameValidator,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPageUsername,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 16.0),
            child: TextFormField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) {
                FocusScope.of(context)
                    .requestFocus(_signupFormFocusNodes["password"]);
              },
              onSaved: (String realname) {
                _loginFormValues["realname"] = realname;
              },
              focusNode: _signupFormFocusNodes["realname"],
              validator: LoginDataController.nameValidator,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPageRealName,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 16.0),
            child: TextFormField(
              keyboardType: TextInputType.text,
              autocorrect: false,
              obscureText: true,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) {
                FocusScope.of(context)
                    .requestFocus(_signupFormFocusNodes["retypepassword"]);
              },
              onSaved: (String password) {
                _signupFormValues["password"] = password;
              },
              focusNode: _signupFormFocusNodes["password"],
              validator: LoginDataController.passwordValidator,
              controller: _signupPasswordController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPagePassword,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 16.0),
            child: TextFormField(
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.text,
              obscureText: true,
              autocorrect: false,
              onFieldSubmitted: (_) {
                _saveSignupForm(context);
              },
              onSaved: (String retypedPassword) {
                _signupFormValues["retypedpassword"] = retypedPassword;
              },
              focusNode: _signupFormFocusNodes["retypepassword"],
              validator: (String retypedPassword) =>
                  LoginDataController.repeatedPasswordValidator(
                      _signupPasswordController.text, retypedPassword),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: AppLocalizations.of(context).loginPageRetypePassword,
              ),
            ),
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                child: Text(
                  AppLocalizations.of(context).loginPageSwitchToLogin,
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  _changeWidget(_LoginPageMode.login);
                },
              ),
              RaisedButton(
                color: Theme.of(context).primaryColor.withOpacity(0.8),
                textColor: Colors.white,
                child: Text(AppLocalizations.of(context).loginPageProceed),
                onPressed: () {
                  _saveSignupForm(context);
                },
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildDocumentSelection(BuildContext context) {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              _changeWidget(_LoginPageMode.signup);
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: RichText(
            text: TextSpan(children: <TextSpan>[
              TextSpan(
                text: AppLocalizations.of(context)
                        .loginPageDocumentSelectionHeader +
                    "\n",
                style: TextStyle(fontSize: 20),
              ),
              TextSpan(
                  text: AppLocalizations.of(context)
                      .loginPageDocumentSeletionText)
            ]),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: <Widget>[
              Expanded(
                child: RaisedButton(
                  color: Theme.of(context).primaryColor.withOpacity(0.8),
                  textColor: Colors.white,
                  onPressed: () {
                    _lastDocumentType = _DocumentType.document;
                    onlyOnceOpen = true;
                    picturesTaken = [null, null];
                    step = 1;
                    _changeWidget(_LoginPageMode.imageView);
                  },
                  child: Text(AppLocalizations.of(context)
                      .loginPageA4DocumentButtonText),
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Expanded(
                  child: RaisedButton(
                color: Theme.of(context).primaryColor.withOpacity(0.8),
                textColor: Colors.white,
                onPressed: () {
                  _lastDocumentType = _DocumentType.studentIdentityCard;
                  onlyOnceOpen = true;
                  picturesTaken = [null, null];
                  step = 1;
                  _changeWidget(_LoginPageMode.imageView);
                },
                child: Text(AppLocalizations.of(context)
                    .loginPageStudentIdentityCardButtonText),
              ))
            ],
          ),
        )
      ],
      mainAxisSize: MainAxisSize.max,
    );
  }

  Widget _buildImageView(BuildContext context) => Column(
        children: <Widget>[
          Align(
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                _changeWidget(_LoginPageMode.documentSelection);
              },
            ),
            alignment: Alignment.topLeft,
          ),
          TakenImageView(
            documentType: _lastDocumentType,
            onReady: () {
              _changeWidget(_LoginPageMode.signupDone);
            },
            context: context,
            cameraAspectRatio: controller.value.aspectRatio,
          ),
        ],
      );

  Widget _buildSignupDoneView(BuildContext context) => Column(
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                _changeWidget(_LoginPageMode.documentSelection);
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: AppLocalizations.of(context)
                              .loginPageDoneSignupHeader +
                          "\n",
                      style: TextStyle(fontSize: 20)),
                  TextSpan(
                      text:
                          AppLocalizations.of(context).loginPageDoneSignupText)
                ],
              ),
            ),
          ),
          TermsOfServiceSwitchListTile(
            onChanged: (bool newValue) {
              setState(() {
                _termsAccepted = newValue;
              });
            },
            value: _termsAccepted,
            termsAndConditionsLink: "http://google.de",
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            switchColor: Theme.of(context).primaryColor,
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: CompositedTransformTarget(
                    link: this._link,
                    child: LayoutBuilder(builder:
                        (BuildContext context, BoxConstraints constraints) {
                      _buttonWidth = constraints.maxWidth;
                      return _switchToLoginAnimationRunning
                          ? Container(
                              height: Theme.of(context).buttonTheme.height,
                            )
                          : RaisedButton(
                              color: Theme.of(context).primaryColor,
                              textColor: Colors.white,
                              child: Text(AppLocalizations.of(context)
                                  .loginPageSendSignupRequestButtonText),
                              onPressed: () {
                                if (!_termsAccepted) {
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                    backgroundColor: Colors.red,
                                    content: Text(
                                      AppLocalizations.of(context)
                                          .termsAndConditionsSwitchListTilePageYouHaveToAccepttermsAndConditionstext,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ));
                                } else {
                                  if (this._overlayEntry == null) {
                                    this._overlayEntry =
                                        _createOverlayEntry(context);
                                    Overlay.of(context)
                                        .insert(this._overlayEntry);
                                    setState(() {
                                      _switchToLoginAnimationRunning = true;
                                    });
                                    _signupCompleteButtonAnimationController
                                        .forward();
                                  }
                                }
                              },
                            );
                    }),
                  ),
                )
              ],
            ),
          ),
        ],
      );

  OverlayEntry _createOverlayEntry(BuildContext context) {
    return OverlayEntry(
      builder: (BuildContext context) => Positioned(
        width: _buttonWidth +
            Tween<double>(begin: 0, end: MediaQuery.of(context).size.width * 2)
                .animate(_signupDisplayFinishedPage)
                .value,
        child: CompositedTransformFollower(
            link: _link,
            showWhenUnlinked: true,
            child: Center(
              child: Transform.scale(
                scale: (Tween<double>(begin: 0, end: 1)
                            .animate(_signupDisplayFinishedPage)
                            .value *
                        ((MediaQuery.of(context).size.width >
                                    MediaQuery.of(context).size.height
                                ? MediaQuery.of(context).size.width
                                : MediaQuery.of(context).size.height) /
                            70) *
                        1.5) +
                    1,
                child: Container(
                  width: Tween<double>(begin: _buttonWidth, end: 70)
                      .animate(_signupCompleteButtonAnimationController)
                      .value,
                  child: Material(
                    color: Theme.of(context).accentColor,
                    shape: Theme.of(context).buttonTheme.getShape(
                          MaterialButton(onPressed: null),
                        ),
                    elevation: Theme.of(context)
                        .buttonTheme
                        .getElevation(MaterialButton(
                          onPressed: null,
                        )),
                    child: Container(
                      height: Tween<double>(
                              begin: Theme.of(context).buttonTheme.height,
                              end: 70)
                          .animate(_signupCompleteButtonAnimationController)
                          .value,
                      child: Center(
                        child: Tween<double>(begin: 0, end: 10)
                                    .animate(
                                        _signupCompleteButtonAnimationController)
                                    .value >
                                7
                            ? Tween<double>(begin: 0, end: 10)
                                        .animate(_signupDisplayFinishedPage)
                                        .value <=
                                    4
                                ? CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  )
                                : Container()
                            : Text(
                                AppLocalizations.of(context)
                                    .loginPageSendSignupRequestButtonText,
                                style: TextStyle(color: Colors.white),
                              ),
                      ),
                    ),
                  ),
                ),
              ),
            )),
      ),
    );
  }

  Widget _buildScrollableArea(
      {bool scrollable = false,
      @required Widget child,
      bool scrolbar = false}) {
    return scrollable
        ? scrolbar
            ? Scrollbar(
                child: SingleChildScrollView(
                  child: child,
                ),
              )
            : SingleChildScrollView(
                child: child,
              )
        : child;
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData deviceParams = MediaQuery.of(context);

    // if (!controller.value.isInitialized) {
    //   return Scaffold(
    //     body: Center(
    //       child: Text("Waiting for camera..."),
    //     ),
    //   );
    // }

    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: AppScaffold(
        displayDrawer: false,
        body: Builder(builder: (BuildContext context) {
          _signupCompleteListener = (AnimationStatus status) {
            if (status == AnimationStatus.completed) {
              if (!shownErrorMessage) {
                shownErrorMessage = true;
                Timer(Duration(seconds: 1), () {
                  shownErrorMessage = false;
                });
                LoginDataController.signup(
                        pictureUrls: picturesTaken,
                        email: _signupFormValues["email"],
                        password: _signupFormValues["password"],
                        realName: _signupFormValues["realname"],
                        username: _signupFormValues["username"])
                    .then((bool success) {
                  if (success)
                    _signupDisplayFinishedPage.forward();
                  else {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text(AppLocalizations.of(context)
                          .loginPageSignupUnscucessfulSnackbar),
                      backgroundColor: Colors.red,
                    ));
                    _signupCompleteButtonAnimationController.reverse();
                    _changeWidget(_LoginPageMode.signup);
                    _signupPasswordController.clear();
                    _termsAccepted = false;
                    _switchToLoginAnimationRunning = false;
                    _signupCompleteButtonAnimationController.reset();
                    _signupDisplayFinishedPage.reset();
                    _dismissController.reset();
                    _overlayEntry.remove();
                    _overlayEntry = null;
                    _textOverlay = null;
                    _dismissAnimation = null;
                  }
                });
              }
            }
          };
          _signupCompleteButtonAnimationController
              .addStatusListener(_signupCompleteListener);

          return Stack(
            children: [
              Image.asset(
                "assets/images/bookshelf_background.jpg",
                height: deviceParams.size.height,
                width: deviceParams.size.width,
                fit: BoxFit.cover,
              ),
              Container(
                width: deviceParams.size.width,
                height: deviceParams.size.height,
                color: Theme.of(context).primaryColor.withOpacity(0.8),
              ),
              SafeArea(
                child: Center(
                  child: LayoutBuilder(
                    builder: (BuildContext context,
                            BoxConstraints viewportConstraints) =>
                        _buildScrollableArea(
                      scrollable:
                          deviceParams.orientation == Orientation.portrait,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                            minHeight: viewportConstraints.maxHeight),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            deviceParams.orientation == Orientation.landscape
                                ? ConstrainedBox(
                                    constraints: BoxConstraints(maxWidth: 400),
                                    child: Padding(
                                      padding: const EdgeInsets.all(16.0),
                                      child: Image.asset(
                                        "assets/images/logo.png",
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            deviceParams.orientation == Orientation.landscape
                                ? Container(
                                    width: 1.2,
                                    height: 150,
                                    decoration:
                                        BoxDecoration(color: Colors.black45),
                                  )
                                : SizedBox(),
                            Expanded(
                              child: ConstrainedBox(
                                constraints: BoxConstraints(minWidth: 400),
                                child: _buildScrollableArea(
                                  scrolbar: true,
                                  scrollable: deviceParams.orientation ==
                                      Orientation.landscape,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Theme(
                                        data: ThemeData(
                                            accentColor:
                                                Theme.of(context).primaryColor,
                                            primaryColor:
                                                Theme.of(context).accentColor,
                                            errorColor: Colors.red.shade900),
                                        child: Builder(
                                            builder: (BuildContext context) {
                                          Widget oldModeWidget = oldMode ==
                                                  _LoginPageMode.login
                                              ? _buildLoginForm(context)
                                              : oldMode == _LoginPageMode.signup
                                                  ? _buildSignupForm(context)
                                                  : oldMode ==
                                                          _LoginPageMode
                                                              .documentSelection
                                                      ? _buildDocumentSelection(
                                                          context)
                                                      : oldMode ==
                                                              _LoginPageMode
                                                                  .imageView
                                                          ? _buildImageView(
                                                              context)
                                                          : oldMode ==
                                                                  _LoginPageMode
                                                                      .signupDone
                                                              ? _buildSignupDoneView(
                                                                  context)
                                                              : SizedBox();
                                          Widget modeWidget = mode ==
                                                  _LoginPageMode.login
                                              ? _buildLoginForm(context)
                                              : mode == _LoginPageMode.signup
                                                  ? _buildSignupForm(context)
                                                  : mode ==
                                                          _LoginPageMode
                                                              .documentSelection
                                                      ? _buildDocumentSelection(
                                                          context)
                                                      : mode ==
                                                              _LoginPageMode
                                                                  .imageView
                                                          ? _buildImageView(
                                                              context)
                                                          : mode ==
                                                                  _LoginPageMode
                                                                      .signupDone
                                                              ? _buildSignupDoneView(
                                                                  context)
                                                              : SizedBox();

                                          return ConstrainedBox(
                                            constraints: BoxConstraints(
                                                minHeight: viewportConstraints
                                                    .maxHeight),
                                            child: Stack(
                                                children: <Widget>[
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: SlideTransition(
                                                  position: Tween<Offset>(
                                                          begin: const Offset(
                                                              0, 0),
                                                          end: Offset(0, 1.3))
                                                      .animate(_controller),
                                                  child: ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        minHeight:
                                                            viewportConstraints
                                                                .maxHeight),
                                                    child: Center(
                                                        child: oldModeWidget),
                                                  ),
                                                ),
                                              ),
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: SlideTransition(
                                                  position: Tween<Offset>(
                                                          begin: const Offset(
                                                              0, -1.3),
                                                          end: Offset(0, 0))
                                                      .animate(_controller),
                                                  child: ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        minHeight: viewportConstraints
                                                                .maxHeight +
                                                            (deviceParams
                                                                        .orientation ==
                                                                    Orientation
                                                                        .landscape
                                                                ? 16
                                                                : 0)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        deviceParams.orientation ==
                                                                Orientation
                                                                    .portrait
                                                            ? Padding(
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                        vertical:
                                                                            16),
                                                                child:
                                                                    Image.asset(
                                                                  "assets/images/logo.png",
                                                                  width: 280,
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        deviceParams.orientation ==
                                                                Orientation
                                                                    .landscape
                                                            ? SizedBox(
                                                                height: 16,
                                                              )
                                                            : SizedBox(),
                                                        Center(
                                                            child: modeWidget),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ]
                                                    .map((widget) =>
                                                        ConstrainedBox(
                                                          child: widget,
                                                          constraints: BoxConstraints(
                                                              minHeight:
                                                                  viewportConstraints
                                                                      .maxHeight),
                                                        ))
                                                    .toList()),
                                          );
                                        }),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}

class TakenImageView extends StatefulWidget {
  final _DocumentType documentType;
  final Function onReady;
  final BuildContext context;
  final double cameraAspectRatio;
  TakenImageView(
      {@required this.documentType,
      @required this.onReady,
      @required this.context,
      @required this.cameraAspectRatio});

  @override
  State<StatefulWidget> createState() {
    return _TakenImageViewState(documentType, context);
  }
}

class _TakenImageViewState extends State<TakenImageView> {
  Future<File> _getLocalFile(String filename) async {
    String dir = (await pathProvider.getTemporaryDirectory()).path;
    File f = new File('$dir/$filename');
    return f;
  }

  _TakenImageViewState(_DocumentType _documentType, BuildContext context) {
    aspectRatio = _documentType == _DocumentType.studentIdentityCard
        ? 1.417
        : _documentType == _DocumentType.document ? 210 / 297 : 0;
  }

  void openCameraPage(
      {int redoIndex,
      _DocumentType documentType,
      @required BuildContext context}) {
    Navigator.pushNamed(context, "/login_camera",
            arguments: documentCameraPage.PageSettings(
                aspectRatio: aspectRatio,
                helperText: (documentType == null
                            ? widget.documentType
                            : documentType) ==
                        _DocumentType.document
                    ? AppLocalizations.of(context)
                        .loginPageA4DocumentPictureHelperText
                    : (documentType == null
                                ? widget.documentType
                                : documentType) ==
                            _DocumentType.studentIdentityCard
                        ? (redoIndex == null ? step : redoIndex) == 1
                            ? AppLocalizations.of(context)
                                .loginPageStudentIdentityCardOuterPictureHelperText
                            : AppLocalizations.of(context)
                                .loginPageStudentIdentityCardInnerPictureHelperText
                        : ""))
        .then((passed) {
      setState(() {
        picturesTaken[(redoIndex == null ? step : redoIndex) - 1] = passed;
      });
    });
  }

  Map<String, Map<String, dynamic>> expansionPanels = {
    "identityCardOuter": {"isExpanded": false, "title": ""},
    "identityCardInner": {"isExpanded": false, "title": ""},
    "document": {"isExpanded": false, "title": ""}
  };

  ExpansionPanel _buildExpansionPanel(String type, int index) => ExpansionPanel(
        isExpanded: expansionPanels[type]["isExpanded"],
        headerBuilder: (BuildContext context, bool isExpanded) => ListTile(
          title: Text(expansionPanels[type]["title"]),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: FutureBuilder(
            future: _getLocalFile(picturesTaken[index - 1]),
            builder: (BuildContext context, AsyncSnapshot snapshot) =>
                snapshot.data == null
                    ? Text("Waiting for picture")
                    : Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          AspectRatio(
                              aspectRatio: widget.cameraAspectRatio,
                              child: Image.file(snapshot.data)),
                          ButtonBar(
                            children: <Widget>[
                              RaisedButton(
                                color: Theme.of(context).primaryColor,
                                textColor: Colors.white,
                                child: Text(AppLocalizations.of(context)
                                    .loginPageRedoImage),
                                onPressed: () {
                                  openCameraPage(
                                      redoIndex: index, context: context);
                                },
                              )
                            ],
                          )
                        ],
                      ),
          ),
        ),
      );

  @override
  void initState() {
    super.initState();
    if (SchedulerBinding.instance.schedulerPhase ==
            SchedulerPhase.persistentCallbacks &&
        onlyOnceOpen) {
      SchedulerBinding.instance
          .addPostFrameCallback((_) => openCameraPage(context: context));
      onlyOnceOpen = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    expansionPanels["identityCardOuter"]["title"] = AppLocalizations.of(context)
        .loginPageOuterPictureOfStudentIdentityCardTitle;
    expansionPanels["identityCardInner"]["title"] = AppLocalizations.of(context)
        .loginPageInnerPictureOfStudentIdentityCardTitle;
    expansionPanels["document"]["title"] =
        AppLocalizations.of(context).loginPagePictureOfDocument;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ExpansionPanelList(
            expansionCallback: (int index, bool isExpanded) {
              setState(() {
                if (widget.documentType == _DocumentType.document &&
                    index == 0) {
                  expansionPanels["document"]["isExpanded"] = !isExpanded;
                  expansionPanels["identityCardOuter"]["isExpanded"] = false;
                  expansionPanels["identityCardInner"]["isExpanded"] = false;
                } else if (widget.documentType ==
                        _DocumentType.studentIdentityCard &&
                    index == 0) {
                  expansionPanels["document"]["isExpanded"] = false;
                  expansionPanels["identityCardOuter"]["isExpanded"] =
                      !isExpanded;
                  expansionPanels["identityCardInner"]["isExpanded"] = false;
                } else if (widget.documentType ==
                        _DocumentType.studentIdentityCard &&
                    index == 1) {
                  expansionPanels["document"]["isExpanded"] = false;
                  expansionPanels["identityCardOuter"]["isExpanded"] = false;
                  expansionPanels["identityCardInner"]["isExpanded"] =
                      !isExpanded;
                }
              });
            },
            children: widget.documentType == _DocumentType.document &&
                    step == 1 &&
                    picturesTaken[0] != null
                ? <ExpansionPanel>[_buildExpansionPanel("document", 1)]
                : widget.documentType == _DocumentType.studentIdentityCard &&
                        step == 1 &&
                        picturesTaken[0] != null
                    ? <ExpansionPanel>[
                        _buildExpansionPanel("identityCardOuter", 1)
                      ]
                    : widget.documentType ==
                                _DocumentType.studentIdentityCard &&
                            step == 2 &&
                            picturesTaken[0] != null &&
                            picturesTaken[1] != null
                        ? <ExpansionPanel>[
                            _buildExpansionPanel("identityCardOuter", 1),
                            _buildExpansionPanel("identityCardInner", 2)
                          ]
                        : <ExpansionPanel>[],
          ),
        ),
        ButtonBar(
          children: <Widget>[
            RaisedButton(
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text(widget.documentType ==
                          _DocumentType.studentIdentityCard &&
                      step == 1
                  ? AppLocalizations.of(context).loginPageTakeNextPicture
                  : AppLocalizations.of(context).loginPageCheckedImagesProceed),
              onPressed: () {
                if (widget.documentType == _DocumentType.studentIdentityCard &&
                    step != 2) {
                  step++;
                  expansionPanels["document"]["isExpanded"] = false;
                  expansionPanels["identityCardOuter"]["isExpanded"] = false;
                  expansionPanels["identityCardInner"]["isExpanded"] = false;
                  openCameraPage(context: context);
                } else {
                  widget.onReady();
                }
              },
            )
          ],
        )
      ],
    );
  }
}

enum _LoginPageMode {
  signup,
  login,
  documentSelection,
  none,
  imageView,
  signupDone
}

enum _DocumentType { studentIdentityCard, unset, document }
