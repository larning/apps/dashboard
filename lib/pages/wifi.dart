import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:igplaner_mobile/controllers/network/mapsController.dart';
import 'package:igplaner_mobile/controllers/scaffold/appScaffold.dart';
import 'package:igplaner_mobile/pages/wifi/wifiDetailsPage.dart';
import 'package:igplaner_mobile/widgets/FlexibleHeightGrid.dart';

import '../localization/localizations.dart';
import '../controllers/wifi/wifiDataController.dart';

class WifiPage extends StatefulWidget {
  static const routeName = "/wifi";

  @override
  _WifiPageState createState() => _WifiPageState();
}

class _WifiPageState extends State<WifiPage> {
  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> _snapshotData;

    return AppScaffold(
      appBar: (context) => AppBar(
        title: Text(AppLocalizations.of(context).wlanPagePageTitle),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: FutureBuilder(
            future: WifiDataController.getWifiData(context),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data != null) {
                _snapshotData =
                    (snapshot.data as List<Map<String, dynamic>>).map((_data) {
                  _data["EXPIRED_BECAUSE"] =
                      _data["DEACTIVATION_MESSAGE"] == null
                          ? ""
                          : _data["DEACTIVATION_MESSAGE"];
                  _data["EXPIRED"] =
                      _data["EXPIRED"] == null ? false : _data["EXPIRED"];
                  _data["PASSWORD_VISIBLE"] = false;
                  return _data;
                }).toList();
              }
              return snapshot.data == null
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : new WifiPanelList(snapshotData: _snapshotData);
            }),
      ),
    );
  }
}

class WifiPanelList extends StatefulWidget {
  const WifiPanelList({this.snapshotData});

  final List<Map<String, dynamic>> snapshotData;

  @override
  _WifiPanelListState createState() => _WifiPanelListState();
}

class _WifiPanelListState extends State<WifiPanelList> {
  List<Map<String, dynamic>> snapshotData;
  @override
  void initState() {
    snapshotData = widget.snapshotData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int id = 0;
    return FlexibleHeightGrid(
        maxCrossAxisExtend: 600,
        children: snapshotData.map((_data) {
          id++;
          return WifiPanel(
            id: id,
            data: _data,
            setPasswordVisible: (bool passwordVisible) {
              setState(() {
                _data["PASSWORD_VISIBLE"] = passwordVisible;
              });
            },
          );
        }).toList());
  }
}

class WifiPanel extends StatelessWidget {
  WifiPanel({this.data, this.setPasswordVisible, this.id});

  final int id;
  final Map<String, dynamic> data;
  final Function(bool) setPasswordVisible;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 8.0,
        ),
        Card(
          child: Hero(
            tag: id,
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, WifiDetailsPage.routeName,
                      arguments:
                          WifiDetailsPageArguments(heroId: id, data: data));
                },
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    LayoutBuilder(builder: (context, constraints) {
                      return Stack(
                        children: <Widget>[
                          Image.network(
                            MapsProvider.of(context).staticImageUrl(
                                longitude: data["COORDINATES"]["LONGITUDE"],
                                latitude: data["COORDINATES"]["LATITUDE"],
                                zoom: 16,
                                context: context),
                            fit: BoxFit.cover,
                            width: constraints.maxWidth < 512
                                ? 512
                                : constraints.maxWidth,
                            height: 200,
                            alignment: Alignment.center,
                          ),
                          Positioned(
                            right: 0,
                            bottom: 0,
                            child: Container(
                              alignment: Alignment.bottomRight,
                              color: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? Colors.grey
                                  : Colors.white60,
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                  "© Stadia Maps, © OpenMapTiles, © OpenStreetMap",
                                  textScaleFactor: 0.7,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }),
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.transparent,
                        backgroundImage: NetworkImage(MapsProvider.of(context)
                            .staticImageUrl(
                                longitude: data["COORDINATES"]["LONGITUDE"],
                                latitude: data["COORDINATES"]["LATITUDE"],
                                zoom: 16,
                                context: context)),
                      ),
                      title: Text(data["SSID"]),
                      subtitle: Text(
                          AppLocalizations.of(context).wlanPageWifiNameSSID),
                    )
                    // ListTile(
                    //   title: Text(AppLocalizations.of(context).wlanPageUsername),
                    //   subtitle: Text(data["USERNAME"]),
                    // ),
                    // ListTile(
                    //   title: Text(AppLocalizations.of(context).wlanPagePassword),
                    //   subtitle: Text(data["PASSWORD_VISIBLE"]
                    //       ? data["PASSWORD"]
                    //       : (data["PASSWORD"] as String)
                    //           .split("")
                    //           .map((it) => "*")
                    //           .join("")),
                    //   trailing: IconButton(
                    //     onPressed: () {
                    //       setPasswordVisible(!data["PASSWORD_VISIBLE"]);
                    //     },
                    //     icon: Icon(data["PASSWORD_VISIBLE"]
                    //         ? Icons.visibility_off
                    //         : Icons.visibility),
                    //   ),
                    // ),
                    // ListTile(
                    //   title: Text(AppLocalizations.of(context).wlanPageStatusTitle),
                    //   subtitle: Text(data["EXPIRED"]
                    //       ? AppLocalizations.of(context).wlanPageExpiredText
                    //       : AppLocalizations.of(context).wlanPageActiveText),
                    // ),
                    // ListTile(
                    //   title: Text(data["EXPIRED"]
                    //       ? AppLocalizations.of(context).wlanPageExpiredBecause
                    //       : AppLocalizations.of(context).wlanPageExpires),
                    //   subtitle: Text(data["EXPIRED"]
                    //       ? data["EXPIRED_BECAUSE"].trim() == ""
                    //           ? "?"
                    //           : data["EXPIRED_BECAUSE"].trim()
                    //       : data["EXPIRES_IN"].toString().trim() == ""
                    //           ? "Doesn't expire" //TODO: Replace placeholder @low
                    //           : SecondsToString(data["EXPIRES_IN"], context)
                    //               .withoutZero()),
                    // ),
                    // ButtonBar(
                    //   children: <Widget>[
                    //     1 == 1
                    //         ? Container()
                    //         : Text(AppLocalizations.of(context)
                    //             .wlanPageAlreadyConnected),
                    //     Builder(builder: (BuildContext context) {
                    //       return RaisedButton(
                    //         onPressed: () {
                    //           Scaffold.of(context).showSnackBar(SnackBar(
                    //             content: Text("Not implemented " +
                    //                 (Platform.isIOS
                    //                     ? "on IOS"
                    //                     : Platform.isAndroid
                    //                         ? "on Android"
                    //                         : "on this Platform")), //
                    //           ));
                    //         },
                    //         color: Theme.of(context).accentColor,
                    //         child: Text(AppLocalizations.of(context)
                    //             .wlanPageConnectNowButtontext),
                    //       );
                    //     })
                    //   ],
                    // )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
