import 'package:flutter/material.dart';
import 'package:igplaner_mobile/controllers/network/mapsController.dart';
import 'package:igplaner_mobile/controllers/scaffold/appScaffold.dart';
import 'package:igplaner_mobile/localization/localizations.dart';

class WifiDetailsPageArguments {
  final int heroId;
  final Map<String, dynamic> data;

  WifiDetailsPageArguments({this.heroId, this.data});
}

class WifiDetailsPage extends StatelessWidget {
  static const routeName = "/wifi/details";

  WifiDetailsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final WifiDetailsPageArguments args =
        ModalRoute.of(context).settings.arguments;

    final Map<String, dynamic> data = args.data;
    final id = args.heroId;

    return AppScaffold(
      subPage: true,
      extendBodyBehindAppBar: true,
      // appBar: (context) => AppBar(
      //   iconTheme: IconThemeData(
      //       color: Theme.of(context).brightness == Brightness.dark
      //           ? Colors.white
      //           : Theme.of(context).textTheme.headline1.color),
      //   elevation: 0.0,
      //   backgroundColor: Colors.transparent,
      // ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              LayoutBuilder(builder: (context, constraints) {
                return Hero(
                  flightShuttleBuilder: (flightContext, animation, direction,
                          fromContext, toContext) =>
                      LayoutBuilder(builder: (context, mainConstraints) {
                    return Material(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          LayoutBuilder(builder: (context, constraints) {
                            return Stack(
                              children: <Widget>[
                                Image.network(
                                  MapsProvider.of(context).staticImageUrl(
                                      longitude: data["COORDINATES"]
                                          ["LONGITUDE"],
                                      latitude: data["COORDINATES"]["LATITUDE"],
                                      zoom: 16,
                                      context: context),
                                  fit: BoxFit.cover,
                                  width: constraints.maxWidth,
                                  height: mainConstraints.maxHeight - 72,
                                  alignment: Alignment.center,
                                ),
                                Positioned(
                                  right: 0,
                                  bottom: 0,
                                  child: Container(
                                    alignment: Alignment.bottomRight,
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? Colors.grey
                                        : Colors.white60,
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Text(
                                        "© Stadia Maps, © OpenMapTiles, © OpenStreetMap",
                                        textScaleFactor: 0.7,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          }),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              foregroundColor: Colors.transparent,
                              backgroundImage: NetworkImage(
                                  MapsProvider.of(context).staticImageUrl(
                                      longitude: data["COORDINATES"]
                                          ["LONGITUDE"],
                                      latitude: data["COORDINATES"]["LATITUDE"],
                                      zoom: 16,
                                      context: context)),
                            ),
                            title: Text(data["SSID"]),
                            subtitle: Text(AppLocalizations.of(context)
                                .wlanPageWifiNameSSID),
                          )
                        ],
                      ),
                    );
                  }),
                  tag: id,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      LayoutBuilder(builder: (context, constraints) {
                        return Stack(
                          children: <Widget>[
                            Image.network(
                              MapsProvider.of(context).staticImageUrl(
                                  longitude: data["COORDINATES"]["LONGITUDE"],
                                  latitude: data["COORDINATES"]["LATITUDE"],
                                  zoom: 16,
                                  context: context),
                              fit: BoxFit.cover,
                              width: constraints.maxWidth,
                              height: constraints.maxWidth < 350
                                  ? constraints.maxWidth
                                  : 350,
                              alignment: Alignment.center,
                            ),
                            Positioned(
                              right: 0,
                              bottom: 0,
                              child: Container(
                                alignment: Alignment.bottomRight,
                                color: Theme.of(context).brightness ==
                                        Brightness.dark
                                    ? Colors.grey
                                    : Colors.white60,
                                child: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Text(
                                    "© Stadia Maps, © OpenMapTiles, © OpenStreetMap",
                                    textScaleFactor: 0.7,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      }),
                      ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          foregroundColor: Colors.transparent,
                          backgroundImage: NetworkImage(MapsProvider.of(context)
                              .staticImageUrl(
                                  longitude: data["COORDINATES"]["LONGITUDE"],
                                  latitude: data["COORDINATES"]["LATITUDE"],
                                  zoom: 16,
                                  context: context)),
                        ),
                        title: Text(data["SSID"]),
                        subtitle: Text(
                            AppLocalizations.of(context).wlanPageWifiNameSSID),
                      )
                      // ListTile(
                      //   title: Text(AppLocalizations.of(context).wlanPageUsername),
                      //   subtitle: Text(data["USERNAME"]),
                      // ),
                      // ListTile(
                      //   title: Text(AppLocalizations.of(context).wlanPagePassword),
                      //   subtitle: Text(data["PASSWORD_VISIBLE"]
                      //       ? data["PASSWORD"]
                      //       : (data["PASSWORD"] as String)
                      //           .split("")
                      //           .map((it) => "*")
                      //           .join("")),
                      //   trailing: IconButton(
                      //     onPressed: () {
                      //       setPasswordVisible(!data["PASSWORD_VISIBLE"]);
                      //     },
                      //     icon: Icon(data["PASSWORD_VISIBLE"]
                      //         ? Icons.visibility_off
                      //         : Icons.visibility),
                      //   ),
                      // ),
                      // ListTile(
                      //   title: Text(AppLocalizations.of(context).wlanPageStatusTitle),
                      //   subtitle: Text(data["EXPIRED"]
                      //       ? AppLocalizations.of(context).wlanPageExpiredText
                      //       : AppLocalizations.of(context).wlanPageActiveText),
                      // ),
                      // ListTile(
                      //   title: Text(data["EXPIRED"]
                      //       ? AppLocalizations.of(context).wlanPageExpiredBecause
                      //       : AppLocalizations.of(context).wlanPageExpires),
                      //   subtitle: Text(data["EXPIRED"]
                      //       ? data["EXPIRED_BECAUSE"].trim() == ""
                      //           ? "?"
                      //           : data["EXPIRED_BECAUSE"].trim()
                      //       : data["EXPIRES_IN"].toString().trim() == ""
                      //           ? "Doesn't expire" //TODO: Replace placeholder @low
                      //           : SecondsToString(data["EXPIRES_IN"], context)
                      //               .withoutZero()),
                      // ),
                      // ButtonBar(
                      //   children: <Widget>[
                      //     1 == 1
                      //         ? Container()
                      //         : Text(AppLocalizations.of(context)
                      //             .wlanPageAlreadyConnected),
                      //     LayoutBuilder(builder: (BuildContext context, _) {
                      //       return RaisedButton(
                      //         onPressed: () {
                      //           Scaffold.of(context).showSnackBar(SnackBar(
                      //             content: Text("Not implemented " +
                      //                 (Platform.isIOS
                      //                     ? "on IOS"
                      //                     : Platform.isAndroid
                      //                         ? "on Android"
                      //                         : "on this Platform")), //
                      //           ));
                      //         },
                      //         color: Theme.of(context).accentColor,
                      //         child: Text(AppLocalizations.of(context)
                      //             .wlanPageConnectNowButtontext),
                      //       );
                      //     })
                      //   ],
                      // )
                    ],
                  ),
                );
              }),
            ],
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: Container(
              height: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.black45, Colors.transparent],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter),
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ),
          )
        ],
      ),
    );
  }
}
