import 'dart:io';

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:native_device_orientation/native_device_orientation.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;

class DocumentCameraPage extends StatefulWidget {
  final Object args;
  final List<CameraDescription> cameras;
  DocumentCameraPage({@required this.args, @required this.cameras});

  @override
  State<StatefulWidget> createState() {
    return _DocumentCameraPageState();
  }
}

class _DocumentCameraPageState extends State<DocumentCameraPage> {
  CameraController controller;

  @override
  void initState() {
    super.initState();
    List<CameraDescription> cameras = widget.cameras;
    controller = CameraController(cameras[0], ResolutionPreset.high);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PageSettings args = widget.args;

    if (!controller.value.isInitialized) {
      return Scaffold(
        body: Center(
          child: Text("Waiting for camera..."),
        ),
      );
    }

    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: NativeDeviceOrientationReader(builder: (BuildContext context) {
            return Stack(
              children: <Widget>[
                LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) =>
                      SizedBox(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight,
                    child: FittedBox(
                      alignment: Alignment.center,
                      fit: BoxFit.cover,
                      child: SizedBox(
                        width: constraints.maxWidth < constraints.maxHeight
                            ? constraints.maxWidth
                            : null,
                        height: constraints.maxWidth > constraints.maxHeight
                            ? constraints.maxHeight
                            : null,
                        child: RotatedBox(
                          quarterTurns: NativeDeviceOrientationReader
                                      .orientation(context) ==
                                  NativeDeviceOrientation.portraitUp
                              ? 0
                              : NativeDeviceOrientationReader.orientation(
                                          context) ==
                                      NativeDeviceOrientation.landscapeLeft
                                  ? 3
                                  : NativeDeviceOrientationReader.orientation(
                                              context) ==
                                          NativeDeviceOrientation.landscapeRight
                                      ? 1
                                      : 2,
                          child: AspectRatio(
                            aspectRatio: controller.value.aspectRatio,
                            child: CameraPreview(controller),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  final double width = constraints.maxWidth;
                  final double height = constraints.maxHeight;

                  final double aspectRatio = args.aspectRatio;

                  double horizontalBorder;
                  double verticalBorder;

                  if (width > height) {
                    horizontalBorder = 32;
                    verticalBorder = (width - (height * aspectRatio)) / 2;
                    verticalBorder += 32;
                  } else {
                    verticalBorder = 32;
                    horizontalBorder = (height - (width * aspectRatio)) / 2;
                    horizontalBorder += 32;
                  }

                  BorderSide horizontal = BorderSide(
                      color: Colors.black54, width: horizontalBorder);

                  BorderSide vertical =
                      BorderSide(color: Colors.black54, width: verticalBorder);

                  BorderSide verticalRightPadding = BorderSide(
                      color: Colors.black54,
                      width: verticalBorder > 50
                          ? verticalBorder + 50
                          : verticalBorder);

                  BorderSide verticalLeftPadding = BorderSide(
                      color: Colors.black54,
                      width: verticalBorder > 50
                          ? verticalBorder - 50
                          : verticalBorder);

                  BorderSide horizontalBottomPadding = BorderSide(
                      color: Colors.black54,
                      width: horizontalBorder > 50
                          ? horizontalBorder + 50
                          : horizontalBorder);

                  BorderSide horizontalTopPadding = BorderSide(
                      color: Colors.black54,
                      width: horizontalBorder > 50
                          ? horizontalBorder - 50
                          : horizontalBorder);

                  final bool landscape = MediaQuery.of(context).orientation ==
                      Orientation.landscape;

                  Border border = Border(
                      right: landscape ? verticalRightPadding : vertical,
                      left: landscape ? verticalLeftPadding : vertical,
                      top: !landscape ? horizontalTopPadding : horizontal,
                      bottom:
                          !landscape ? horizontalBottomPadding : horizontal);

                  return Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: height,
                      width: width,
                      decoration: BoxDecoration(
                        border: border,
                      ),
                      child: SizedBox(),
                    ),
                  );
                }),
                Align(
                  alignment: MediaQuery.of(context).orientation ==
                          Orientation.landscape
                      ? Alignment.centerRight
                      : Alignment.bottomCenter,
                  child: SizedBox(
                    height: MediaQuery.of(context).orientation ==
                            Orientation.landscape
                        ? double.infinity
                        : 100,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? double.infinity
                        : 100,
                    child: Container(
                      color: Theme.of(context).accentColor.withOpacity(0.6),
                      child: Center(
                        child: FloatingActionButton(
                          child: Icon(Icons.camera),
                          backgroundColor: Theme.of(context).primaryColor,
                          onPressed: () {
                            String timestamp = DateTime.now().toIso8601String();

                            pathProvider
                                .getTemporaryDirectory()
                                .then((Directory dir) {
                              controller
                                  .takePicture(
                                      "${dir.path}/igplaner_document_image$timestamp.jpg")
                                  .then((_) {
                                Navigator.pop(context,
                                    "igplaner_document_image$timestamp.jpg");
                              });
                            });
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: MediaQuery.of(context).orientation ==
                          Orientation.landscape
                      ? Alignment.bottomCenter
                      : Alignment.topCenter,
                  child: SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: Text(
                        args.helperText,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                )
              ],
            );
          }),
        ),
      ),
    );
  }
}

class PageSettings {
  final double aspectRatio;
  final String helperText;
  PageSettings({@required this.aspectRatio, @required this.helperText});
}
